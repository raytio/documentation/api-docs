{
    "schema_name": "ss_prm_location",
    "schema_version": "0.0.11",
    "schema_type": "ss",
    "schema": {
        "schema_group": "contact_information",
        "tags": [
            "type:client_only"
        ],
        "i18n": {
            "en": {
                "contact_information": {
                    "title_plural": "Contact information",
                    "description": "Contact information",
                    "title": "Contact information"
                },
                "summary": {
                    "title_plural": "Summaries",
                    "description": "Summary information",
                    "title": "Summary"
                },
                "advanced": {
                    "title_plural": "Advanced",
                    "description": "Advanced information",
                    "title": "Advanced"
                },
                "address": {
                    "title_plural": "Addresses",
                    "description": "Address information",
                    "title": "Address"
                }
            }
        },
        "database": {
            "table": "prm_locations",
            "primary_key": "location_number"
        },
        "definitions": {
            "ss_location_sublocality_string": {
                "$ref": "urn:schema:ss_location_sublocality_string"
            },
            "ss_location_postal_code_string": {
                "$ref": "urn:schema:ss_location_postal_code_string"
            },
            "ss_location_postal_code_suffix_string": {
                "$ref": "urn:schema:ss_location_postal_code_suffix_string"
            },
            "ss_location_locality_string": {
                "$ref": "urn:schema:ss_location_locality_string"
            },
            "ss_location_premise_string": {
                "$ref": "urn:schema:ss_location_premise_string"
            },
            "ss_location_subpremise_string": {
                "$ref": "urn:schema:ss_location_subpremise_string"
            },
            "ss_location_street_number_string": {
                "$ref": "urn:schema:ss_location_street_number_string"
            },
            "ss_location_route_string": {
                "$ref": "urn:schema:ss_location_route_string"
            },
            "ss_location_administrative_area_level_1_string": {
                "$ref": "urn:schema:ss_location_administrative_area_level_1_string"
            },
            "ss_location_administrative_area_level_2_string": {
                "$ref": "urn:schema:ss_location_administrative_area_level_2_string"
            },
            "ss_location_administrative_area_level_3_string": {
                "$ref": "urn:schema:ss_location_administrative_area_level_3_string"
            }
        },
        "description": "Your physical address details",
        "display": {
            "head_main": {
                "fields": [
                    "location_number",
                    "location_name",
                    "locality"
                ]
            },
            "head_sub": {
                "fields": [
                    "country"
                ]
            },
            "tabular": {
                "fields": [
                    "location_number",
                    "location_name",
                    "locality",
                    "country"
                ]
            }
        },
        "properties": {
            "start_date": {
                "title": "Start date",
                "description": "The date from which this location can be used",
                "type": "string",
                "format": "date-time",
                "priority": 1200,
                "tags": [
                    "group:summary"
                ]
            },
            "end_date": {
                "title": "End date",
                "description": "The date after which this location can no longer be used",
                "type": "string",
                "format": "date-time",
                "priority": 1300,
                "tags": [
                    "group:summary"
                ]
            },
            "active": {
                "title": "Active",
                "description": "Whether this location can be used or not",
                "type": "boolean",
                "priority": 1400,
                "tags": [
                    "group:summary"
                ]
            },
            "route": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_route_string"
                    },
                    {
                        "priority": 50,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "administrative_area_level_2": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_administrative_area_level_2_string"
                    },
                    {
                        "priority": 140,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "administrative_area_level_3": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_administrative_area_level_3_string"
                    },
                    {
                        "priority": 130,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "postal_code": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_postal_code_string"
                    },
                    {
                        "priority": 180,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "postal_code_suffix": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_postal_code_suffix_string"
                    },
                    {
                        "priority": 190,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "country": {
                "type": "string",
                "title": "Location country",
                "description": "The country of this location",
                "lookup": "{API_BASE_URL}/db/v1/fnd_countries?select=key:iso_country_code_2,value:country_name",
                "tags": [
                    "group:address",
                    "display:no_autofill"
                ],
                "priority": 200
            },
            "premise": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_premise_string"
                    },
                    {
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ],
                        "priority": 30
                    }
                ]
            },
            "subpremise": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_subpremise_string"
                    },
                    {
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ],
                        "priority": 20
                    }
                ]
            },
            "street_number": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_street_number_string"
                    },
                    {
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ],
                        "priority": 40
                    }
                ]
            },
            "locality": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_locality_string"
                    },
                    {
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ],
                        "priority": 70
                    }
                ]
            },
            "sublocality": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_sublocality_string"
                    },
                    {
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ],
                        "priority": 60
                    }
                ]
            },
            "geolocation": {
                "type": "object",
                "description": "The geographic location of the address",
                "title": "Geolocation",
                "priority": 200,
                "properties": {
                    "latitude": {
                        "type": "number"
                    },
                    "longitude": {
                        "type": "number"
                    },
                    "accuracy": {
                        "type": "number"
                    }
                },
                "readOnly": true
            },
            "location_name": {
                "type": "string",
                "title": "Location name",
                "description": "Your reference or nickname for this location",
                "tags": [
                    "group:summary"
                ],
                "priority": 5000
            },
            "location_description": {
                "type": "string",
                "title": "Location description",
                "description": "A description of this location",
                "tags": [
                    "group:summary"
                ],
                "priority": 5100
            },
            "location_number": {
                "type": "integer",
                "title": "Location number",
                "description": "The unique number for this location",
                "priority": 5200,
                "tags": [
                    "group:summary"
                ],
                "readOnly": true
            },
            "id": {
                "type": "string",
                "title": "Unique ID",
                "description": "The unique identifier for this location",
                "priority": 5300,
                "tags": [
                    "group:advanced"
                ],
                "readOnly": true
            }
        },
        "allOf": [
            {
                "if": {
                    "properties": {
                        "country": {
                            "enum": [
                                "AU"
                            ]
                        }
                    }
                },
                "then": {
                    "required": [
                        "locality",
                        "administrative_area_level_1",
                        "country"
                    ],
                    "properties": {
                        "administrative_area_level_1": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/ss_location_administrative_area_level_1_string"
                                },
                                {
                                    "priority": 150,
                                    "tags": [
                                        "group:address",
                                        "display:no_autofill"
                                    ],
                                    "lookup": "{API_DOCS_URL}/lookups/au_states.json"
                                }
                            ]
                        }
                    }
                }
            }
        ],
        "title": "Physical Address",
        "title_plural": "Physical Addresses",
        "$schema": "http://json-schema.org/draft-07/schema#",
        "required": [
            "locality",
            "country"
        ],
        "type": "object"
    }
}