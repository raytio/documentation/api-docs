{
    "schema": {
        "schema_group": "financial_information",
        "i18n": {
            "en": {
                "financial_information": {
                    "title_plural": "Financial information",
                    "description": "Financial information",
                    "title": "Financial information"
                },
                "group:name:person_name.field": {
                    "title_plural": "Person names",
                    "description": "The full name of the person",
                    "title": "Person Name"
                },
                "group:date_picker:birth.field": {
                    "title_plural": "Birth dates",
                    "description": "The birth date of the person",
                    "title": "Birth date"
                }
            }
        },
        "definitions": {
            "ss_first_given_name_string": {
                "$ref": "urn:schema:ss_first_given_name_string"
            },
            "ss_gender_string": {
                "$ref": "urn:schema:ss_gender_string"
            },
            "ss_birth_year_integer": {
                "$ref": "urn:schema:ss_birth_year_integer"
            },
            "ss_birth_place_string": {
                "$ref": "urn:schema:ss_birth_place_string"
            },
            "ss_birth_day_integer": {
                "$ref": "urn:schema:ss_birth_day_integer"
            },
            "ss_birth_month_integer": {
                "$ref": "urn:schema:ss_birth_month_integer"
            },
            "ss_family_name_string": {
                "$ref": "urn:schema:ss_family_name_string"
            },
            "ss_other_given_names_string": {
                "$ref": "urn:schema:ss_other_given_names_string"
            },
            "ss_location_sublocality_string": {
                "$ref": "urn:schema:ss_location_sublocality_string"
            },
            "ss_location_postal_code_string": {
                "$ref": "urn:schema:ss_location_postal_code_string"
            },
            "ss_location_postal_code_suffix_string": {
                "$ref": "urn:schema:ss_location_postal_code_suffix_string"
            },
            "ss_location_locality_string": {
                "$ref": "urn:schema:ss_location_locality_string"
            },
            "ss_location_premise_string": {
                "$ref": "urn:schema:ss_location_premise_string"
            },
            "ss_location_subpremise_string": {
                "$ref": "urn:schema:ss_location_subpremise_string"
            },
            "ss_location_street_number_string": {
                "$ref": "urn:schema:ss_location_street_number_string"
            },
            "ss_location_route_string": {
                "$ref": "urn:schema:ss_location_route_string"
            },
            "ss_location_administrative_area_level_1_string": {
                "$ref": "urn:schema:ss_location_administrative_area_level_1_string"
            },
            "ss_location_administrative_area_level_2_string": {
                "$ref": "urn:schema:ss_location_administrative_area_level_2_string"
            },
            "ss_location_administrative_area_level_3_string": {
                "$ref": "urn:schema:ss_location_administrative_area_level_3_string"
            }
        },
        "$schema": "http://json-schema.org/draft-07/schema#",
        "type": "object",
        "title": "Credit check",
        "description": "Individual credit check",
        "tags": [
            "action:verify",
            "action:map",
            "link_to:ss_Person_Name:credit_report",
            "time:extract:35",
            "time:verify:15",
            "time:persist:30",
            "time:persist_verify:20"
        ],
        "required": [
            "first_given_name",
            "family_name",
            "birth_day",
            "birth_month",
            "birth_year",
            "gender",
            "street_number",
            "route",
            "locality",
            "country"
        ],
        "verified_fields": [
            "first_given_name",
            "family_name",
            "gender",
            "street_number",
            "route",
            "locality",
            "country",
            "centrix_credit_score"
        ],
        "properties": {
            "centrix_credit_score": {
                "type": "string",
                "title": "Credit score",
                "description": "Centrix credit score",
                "readOnly": true
            },
            "family_name": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_family_name_string"
                    },
                    {
                        "priority": 30,
                        "tags": [
                            "group:name:person_name"
                        ]
                    }
                ]
            },
            "first_given_name": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_first_given_name_string"
                    },
                    {
                        "tags": [
                            "group:name:person_name"
                        ],
                        "priority": 10
                    }
                ]
            },
            "other_given_names": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_other_given_names_string"
                    },
                    {
                        "tags": [
                            "group:name:person_name"
                        ],
                        "priority": 20
                    }
                ]
            },
            "birth_month": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_birth_month_integer"
                    },
                    {
                        "tags": [
                            "group:date_picker:birth",
                            "date_component:month"
                        ],
                        "priority": 50
                    }
                ]
            },
            "birth_year": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_birth_year_integer"
                    },
                    {
                        "priority": 60,
                        "tags": [
                            "group:date_picker:birth",
                            "date_component:year"
                        ]
                    }
                ]
            },
            "birth_day": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_birth_day_integer"
                    },
                    {
                        "priority": 40,
                        "tags": [
                            "group:date_picker:birth",
                            "date_component:day"
                        ]
                    }
                ]
            },
            "gender": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_gender_string"
                    },
                    {
                        "priority": 100,
                        "lookup": "{API_DOCS_URL}/lookups/gender.json"
                    }
                ]
            },
            "route": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_route_string"
                    },
                    {
                        "priority": 150,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "administrative_area_level_1": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_administrative_area_level_1_string"
                    },
                    {
                        "priority": 250,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "administrative_area_level_2": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_administrative_area_level_2_string"
                    },
                    {
                        "priority": 240,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "administrative_area_level_3": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_administrative_area_level_3_string"
                    },
                    {
                        "priority": 230,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "postal_code": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_postal_code_string"
                    },
                    {
                        "priority": 280,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "postal_code_suffix": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_postal_code_suffix_string"
                    },
                    {
                        "priority": 290,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "country": {
                "type": "string",
                "title": "Location country",
                "description": "The country of this location",
                "lookup": "{API_BASE_URL}/db/v1/fnd_countries?select=key:iso_country_code_2,value:country_name",
                "tags": [
                    "group:address",
                    "display:no_autofill"
                ],
                "priority": 300
            },
            "premise": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_premise_string"
                    },
                    {
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ],
                        "priority": 130
                    }
                ]
            },
            "subpremise": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_subpremise_string"
                    },
                    {
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ],
                        "priority": 120
                    }
                ]
            },
            "street_number": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_street_number_string"
                    },
                    {
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ],
                        "priority": 140
                    }
                ]
            },
            "locality": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_locality_string"
                    },
                    {
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ],
                        "priority": 170
                    }
                ]
            },
            "sublocality": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_location_sublocality_string"
                    },
                    {
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ],
                        "priority": 160
                    }
                ]
            }
        }
    },
    "schema_name": "NZ_Centrix_Individual_Credit_Check",
    "schema_version": "0.0.15",
    "schema_type": "ss"
}