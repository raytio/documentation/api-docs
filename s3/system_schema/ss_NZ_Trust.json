{
    "schema_name": "ss_NZ_Trust",
    "schema_version": "0.1.18",
    "schema_type": "ss",
    "schema": {
        "relationships": [
            {
                "direction": "from",
                "type": "HAS_RELATED_DATA",
                "oneOf": [
                    "instance"
                ],
                "multiple": true
            },
            {
                "direction": "from",
                "relationship_name": "settlor",
                "type": "HAS_RELATED_PARTY",
                "oneOf": [
                    "ss_Person_Name"
                ],
                "multiple": true,
                "required": [
                    "role_type"
                ],
                "properties": {
                    "role_type": {
                        "type": "string",
                        "enum": [
                            "Settlor"
                        ],
                        "default": "Settlor"
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "appointer",
                "type": "HAS_RELATED_PARTY",
                "oneOf": [
                    "ss_Person_Name"
                ],
                "multiple": true,
                "required": [
                    "role_type"
                ],
                "properties": {
                    "role_type": {
                        "type": "string",
                        "enum": [
                            "Appointer (Trustee)",
                            "Appointer (Beneficiary)",
                            "Appointer (Trustee and Beneficiary)"
                        ],
                        "priority": 100
                    },
                    "appointment_date": {
                        "type": "string",
                        "format": "date",
                        "default": 0,
                        "priority": 200
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "advisor",
                "type": "HAS_RELATED_PARTY",
                "anyOf": [
                    "ss_Person_Name",
                    "ss_NZ_Company"
                ],
                "multiple": true,
                "required": [
                    "role_type",
                    "appointment_date"
                ],
                "properties": {
                    "role_type": {
                        "type": "string",
                        "enum": [
                            "Advisor"
                        ],
                        "default": "Advisor",
                        "priority": 100
                    },
                    "appointment_date": {
                        "type": "string",
                        "format": "date",
                        "default": 0,
                        "priority": 200
                    },
                    "independent": {
                        "type": "boolean",
                        "default": true,
                        "priority": 300
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "trustee",
                "type": "HAS_RELATED_PARTY",
                "anyOf": [
                    "ss_Person_Name",
                    "ss_NZ_Company"
                ],
                "multiple": true,
                "required": [
                    "role_type",
                    "appointment_date"
                ],
                "required_relationship": true,
                "properties": {
                    "role_type": {
                        "type": "string",
                        "enum": [
                            "Trustee"
                        ],
                        "default": "Trustee",
                        "priority": 100
                    },
                    "appointment_date": {
                        "type": "string",
                        "format": "date",
                        "default": 0,
                        "priority": 200
                    },
                    "independent": {
                        "type": "boolean",
                        "default": true,
                        "priority": 300
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "protector",
                "type": "HAS_RELATED_PARTY",
                "anyOf": [
                    "ss_Person_Name"
                ],
                "multiple": true,
                "required": [
                    "role_type",
                    "appointment_date"
                ],
                "properties": {
                    "role_type": {
                        "type": "string",
                        "enum": [
                            "Protector"
                        ],
                        "default": "Protector",
                        "priority": 100
                    },
                    "appointment_date": {
                        "type": "string",
                        "format": "date",
                        "default": 0,
                        "priority": 200
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "beneficiary",
                "type": "HAS_RELATED_PARTY",
                "oneOf": [
                    "ss_Person_Name",
                    "ss_NZ_Company"
                ],
                "multiple": true,
                "required": [
                    "role_type",
                    "appointment_date"
                ],
                "properties": {
                    "role_type": {
                        "type": "string",
                        "enum": [
                            "Beneficiary"
                        ],
                        "default": "Beneficiary",
                        "priority": 100
                    },
                    "appointment_date": {
                        "type": "string",
                        "format": "date",
                        "default": 0,
                        "priority": 300
                    },
                    "entitlement_date": {
                        "type": "string",
                        "format": "date",
                        "default": 0,
                        "priority": 400
                    },
                    "beneficiary_type": {
                        "type": "string",
                        "enum": [
                            "Specific",
                            "Other",
                            "Discretionary",
                            "Remaining"
                        ],
                        "priority": 200
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "contact",
                "type": "HAS_CONTACT",
                "oneOf": [
                    "ss_Email_Address",
                    "ss_Phone_Number"
                ],
                "multiple": true,
                "properties": {
                    "contact_type": {
                        "type": "string",
                        "enum": [
                            "email",
                            "phone"
                        ],
                        "priority": 100
                    },
                    "start_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0,
                        "priority": 200
                    },
                    "end_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0,
                        "priority": 300
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "related_address",
                "type": "HAS_ADDRESS",
                "oneOf": [
                    "ss_Contact_Address"
                ],
                "multiple": true,
                "required": [
                    "address_type",
                    "start_date"
                ],
                "properties": {
                    "address_type": {
                        "type": "string",
                        "lookup": "{API_DOCS_URL}/lookups/nz_entity_address_types.json",
                        "priority": 100
                    },
                    "start_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0,
                        "priority": 200
                    },
                    "end_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0,
                        "priority": 300
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "trust_deed",
                "type": "HAS_DOCUMENT",
                "oneOf": [
                    "ss_File"
                ],
                "multiple": true,
                "required_relationship": true,
                "required": [
                    "document_date"
                ],
                "properties": {
                    "document_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "funds_source",
                "type": "HAS_DOCUMENT",
                "oneOf": [
                    "ss_File"
                ],
                "multiple": true,
                "required_relationship": true,
                "required": [
                    "document_date",
                    "funds_source"
                ],
                "properties": {
                    "document_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0,
                        "priority": 300
                    },
                    "funds_source": {
                        "type": "string",
                        "lookup": "{API_DOCS_URL}/lookups/nz_funds_source.json",
                        "priority": 100
                    },
                    "funds_source_description": {
                        "type": "string",
                        "priority": 200
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "linked_accounts",
                "type": "HAS_ACCOUNTS",
                "oneOf": [
                    "ss_Connected_Account_Xero"
                ],
                "multiple": false
            },
            {
                "direction": "from",
                "relationship_name": "tax_details",
                "type": "HAS_TAX_DETAILS",
                "required_relationship": true,
                "anyOf": [
                    "ss_Tax_Registration_Details"
                ],
                "multiple": true
            }
        ],
        "type": "object",
        "title": "NZ trust",
        "i18n": {
            "en-nz": {
                "administrator": {
                    "title_plural": "Administrators",
                    "description": "Company administrator",
                    "title": "Administrator"
                },
                "director": {
                    "description": "Company director",
                    "title_plural": "Directors",
                    "title": "Director"
                },
                "role": {
                    "description": "The role",
                    "title": "Role",
                    "title_plural": "Roles"
                },
                "role_type": {
                    "description": "The type of role",
                    "title": "Role Type",
                    "title_plural": "Role Type"
                },
                "appointment_date": {
                    "description": "The date on which appointment was made",
                    "title": "Appointment date",
                    "title_plural": "Appointment dates"
                },
                "independent": {
                    "title": "Independent",
                    "content": "Is independent?",
                    "description": "Is the person or organisation independent?"
                },
                "shareholder": {
                    "title_plural": "Shareholders",
                    "description": "Company shareholder",
                    "title": "Shareholder"
                },
                "$schema": {
                    "title": "Trust",
                    "description": "NZ trust details"
                },
                "nz_legal_entity_name": {
                    "title": "Entity Name",
                    "title_plural": "Entity Names",
                    "description": "The name of this legal entity"
                },
                "nzbn": {
                    "description": "New Zealand Business Number",
                    "title": "NZBN",
                    "title_plural": "NZBNs"
                },
                "contact": {
                    "description": "Company contact",
                    "title_plural": "Contacts",
                    "title": "Contact"
                },
                "related_address": {
                    "description": "Related address",
                    "title_plural": "Related addresses",
                    "title": "Related address"
                },
                "registered_address": {
                    "description": "Registered address",
                    "title_plural": "Registered addresses",
                    "title": "Registered address"
                },
                "service_address": {
                    "description": "Service address",
                    "title_plural": "Service addresses",
                    "title": "Address for service"
                },
                "communications_address": {
                    "description": "Communications address",
                    "title_plural": "Communications addresses",
                    "title": "Address for communications "
                },
                "records_address": {
                    "description": "Records address",
                    "title_plural": "Records addresses",
                    "title": "Address for records"
                },
                "register_address": {
                    "description": "Address for service",
                    "title_plural": "Service addresses",
                    "title": "Address for service"
                },
                "advisor": {
                    "description": "Trust advisor",
                    "title": "Advisor",
                    "title_plural": "Advisors"
                },
                "protector": {
                    "description": "Trust protector",
                    "title": "Protector",
                    "title_plural": "Protectors"
                },
                "trustee": {
                    "description": "Trustee",
                    "title": "Trustee",
                    "title_plural": "Trustees"
                },
                "beneficiary": {
                    "description": "Beneficiary",
                    "title": "Beneficiary",
                    "title_plural": "Beneficiaries"
                },
                "appointer": {
                    "description": "Appointer",
                    "title": "Appointer",
                    "title_plural": "Appointers"
                },
                "settlor": {
                    "description": "Trust settlor",
                    "title": "Settlor",
                    "title_plural": "Settlors"
                },
                "contact_type": {
                    "description": "The purpose for which this contact is used",
                    "title": "Contact type",
                    "title_plural": "Contact types"
                },
                "address_type": {
                    "description": "The purpose for which this address is used",
                    "title": "Address type",
                    "title_plural": "Address types"
                },
                "start_date": {
                    "description": "The start date",
                    "title": "Start date",
                    "title_plural": "Start dates"
                },
                "end_date": {
                    "description": "The end date",
                    "title": "End date",
                    "title_plural": "End dates"
                },
                "group:nz_trust": {
                    "title": "Trust"
                },
                "document_date": {
                    "title": "The date of the document",
                    "title_plural": "Document dates"
                },
                "funds_source": {
                    "description": "The trusts source of wealth or funds",
                    "title": "Sources of wealth or funds",
                    "title_plural": "Source of wealth or funds"
                },
                "trust_deed": {
                    "description": "The trusts source of wealth or funds",
                    "title": "Trust deed and amendments",
                    "title_plural": "Trust documents"
                },
                "funds_source_description": {
                    "description": "A brief description of the funds source",
                    "title": "Funds source",
                    "title_plural": "Funds source"
                },
                "tax_details": {
                    "title_plural": "Tax details",
                    "description": "Local and foreign tax registration details",
                    "title": "Tax details"
                },
                "linked_accounts": {
                    "title_plural": "Accounts",
                    "description": "Financial accounting records",
                    "title": "Accounts"
                }
            }
        },
        "definitions": {
            "ss_nz_legal_entity_name_string": {
                "$ref": "urn:schema:ss_nz_legal_entity_name_string"
            },
            "ss_nz_nzbn_string": {
                "$ref": "urn:schema:ss_nz_nzbn_string"
            }
        },
        "properties": {
            "source_register_unique_identifier": {
                "title": "Source register id",
                "tags": [
                    "group:nz_trust"
                ],
                "type": "string",
                "description": "The unique identifier for this entity taken from the source register for this entity type",
                "examples": [
                    "1447962"
                ],
                "priority": 600,
                "encrypt": true
            },
            "source_register": {
                "title": "Source register",
                "tags": [
                    "group:nz_trust"
                ],
                "type": "string",
                "description": "The name of the source register",
                "examples": [
                    "COMPANY"
                ],
                "priority": 700
            },
            "nz_legal_entity_name": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_legal_entity_name_string"
                    },
                    {
                        "tags": [
                            "group:nz_trust"
                        ],
                        "priority": 200
                    }
                ]
            },
            "entity_type_description": {
                "examples": [
                    "NZ Limited Company"
                ],
                "title": "Entity type",
                "description": "The type of legal entity",
                "lookup": "{API_DOCS_URL}/lookups/nz_entity_types.json",
                "type": "string",
                "default": "Trust",
                "tags": [
                    "group:nz_trust"
                ],
                "priority": 300
            },
            "nzbn": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_nzbn_string"
                    },
                    {
                        "priority": 100,
                        "tags": [
                            "group:nz_trust"
                        ]
                    }
                ]
            },
            "entity_status_description": {
                "title": "Entity status",
                "tags": [
                    "group:nz_trust"
                ],
                "examples": [
                    "Removed"
                ],
                "type": "string",
                "priority": 400,
                "lookup": "{API_DOCS_URL}/lookups/nz_entity_status.json",
                "description": "The status of the legal entity"
            },
            "registration_date": {
                "title": "Trust Creation Date",
                "tags": [
                    "group:nz_trust"
                ],
                "examples": [
                    "Removed"
                ],
                "type": "string",
                "format": "date-time",
                "priority": 500,
                "description": "The date on which the trust was first created"
            }
        },
        "description": "NZ trust details",
        "required": [
            "nz_legal_entity_name"
        ],
        "display": {
            "head_sub": {
                "fields": [
                    "entity_type_description"
                ]
            },
            "head_main": {
                "fields": [
                    "nz_legal_entity_name"
                ]
            },
            "expand": [
                {
                    "fields": [
                        "source_register_unique_identifier",
                        "source_register"
                    ],
                    "label": "Registry details for {nzbn}"
                }
            ]
        },
        "verified_fields": [
            "nzbn",
            "nz_legal_entity_name",
            "entity_type_description",
            "entity_status",
            "source_register_unique_identifier"
        ],
        "$schema": "http://json-schema.org/draft-07/schema#"
    }
}