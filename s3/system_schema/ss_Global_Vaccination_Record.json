{
    "schema_name": "ss_Global_Vaccination_Record",
    "schema_type": "ss",
    "schema_version": "0.0.21",
    "schema": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "title": "Vaccination Record",
        "schema_group": "health_information",
        "type": "object",
        "description": "An individual's vaccination record",
        "tags": [
            "action:verify",
            "time:verify:15",
            "time:persist:30",
            "time:persist_verify:20"
        ],
        "verified_fields": [
            "first_given_name",
            "family_name",
            "birth_year",
            "birth_month",
            "birth_day",
            "vaccination_status"
        ],
        "i18n": {
            "en": {
                "health_information": {
                    "title": "Health Information",
                    "title_plural": "Health Information"
                },
                "group:name:person_name.field": {
                    "title_plural": "Person names",
                    "description": "The full name of the person",
                    "title": "Person Name"
                },
                "group:date_picker:birth.field": {
                    "title_plural": "Birth dates",
                    "description": "The birth date of the person",
                    "title": "Birth date"
                },
                "$loading_extract": {
                    "title": "Analyzing image"
                },
                "$loading_verify": {
                    "title": "Verifying document"
                },
                "$schema": {
                    "title": "Vaccination",
                    "description": "Vaccination record"
                },
                "$loading_save": {
                    "title": "Storing document"
                }
            }
        },
        "display": {
            "head_main": {
                "fields": [
                    "first_given_name",
                    "family_name",
                    "dose_sequence",
                    "dose_total"
                ],
                "format": "$0 $1 $2/$3"
            }
        },
        "properties": {
            "birth_day": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_birth_day_integer"
                    },
                    {
                        "priority": 800,
                        "tags": [
                            "group:date_picker:birth",
                            "date_component:day",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "birth_month": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_birth_month_integer"
                    },
                    {
                        "tags": [
                            "group:date_picker:birth",
                            "date_component:month",
                            "display:no_autofill"
                        ],
                        "priority": 900
                    }
                ]
            },
            "birth_year": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_birth_year_integer"
                    },
                    {
                        "priority": 1000,
                        "tags": [
                            "group:date_picker:birth",
                            "date_component:year",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "first_given_name": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_first_given_name_string"
                    },
                    {
                        "tags": [
                            "display:no_autofill",
                            "group:name:person_name"
                        ],
                        "priority": 500
                    }
                ]
            },
            "other_given_names": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_other_given_names_string"
                    },
                    {
                        "tags": [
                            "display:no_autofill",
                            "group:name:person_name"
                        ],
                        "priority": 600
                    }
                ]
            },
            "family_name": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_family_name_string"
                    },
                    {
                        "tags": [
                            "display:no_autofill",
                            "group:name:person_name"
                        ],
                        "priority": 700
                    }
                ]
            },
            "vaccination_proof": {
                "readOnly": true,
                "description": "Scan the vaccination QR code",
                "type": "string",
                "tags": [
                    "group:qr"
                ],
                "contentMediaType": "image",
                "priority": 5200,
                "title": "QR code"
            },
            "vaccination_status": {
                "description": "The individual's vaccination status",
                "type": "string",
                "priority": 5300,
                "title": "Vaccination status"
            },
            "product_name": {
                "type": "object",
                "priority": 6100,
                "title": "Product",
                "description": "Product name",
                "properties": {
                    "code": {
                        "priority": 10,
                        "title": "Code",
                        "description": "Product code",
                        "type": "string"
                    },
                    "name": {
                        "priority": 20,
                        "title": "Name",
                        "description": "Product name",
                        "type": "string"
                    }
                }
            },
            "date": {
                "type": "string",
                "priority": 6200,
                "title": "Date",
                "description": "The date on which the vaccine was administered"
            },
            "manufacturer": {
                "type": "object",
                "priority": 6300,
                "title": "Manufacturer",
                "description": "The manufacturer of the vaccine",
                "properties": {
                    "code": {
                        "type": "string"
                    },
                    "name": {
                        "type": "string"
                    }
                }
            },
            "disease": {
                "type": "object",
                "priority": 6400,
                "title": "Disease",
                "description": "The disease treated by the vaccine",
                "properties": {
                    "code": {
                        "type": "string"
                    },
                    "name": {
                        "type": "string"
                    }
                }
            },
            "vaccine_name": {
                "type": "object",
                "priority": 6500,
                "title": "Vaccine",
                "description": "Vaccine name",
                "properties": {
                    "code": {
                        "type": "string"
                    },
                    "name": {
                        "type": "string"
                    }
                }
            },
            "route": {
                "type": "string",
                "priority": 6600,
                "title": "Administration route",
                "description": "The method of vaccine administration"
            },
            "site": {
                "type": "string",
                "priority": 6700,
                "title": "Site",
                "description": "The site of vaccine administration"
            },
            "location": {
                "type": "string",
                "priority": 6800,
                "title": "Location",
                "description": "Where the vaccincation was administered"
            },
            "location_country": {
                "type": "string",
                "priority": 6900,
                "title": "Country",
                "description": "The country in which the vaccincation was administered",
                "lookup": "{API_BASE_URL}/db/v1/fnd_countries?select=key:iso_country_code_2,value:country_name"
            },
            "dose_sequence": {
                "type": "number",
                "priority": 7000,
                "title": "Dose sequence"
            },
            "dose_total": {
                "type": "number",
                "priority": 7100,
                "title": "Total doses"
            }
        },
        "definitions": {
            "ss_birth_month_integer": {
                "$ref": "urn:schema:ss_birth_month_integer"
            },
            "ss_birth_day_integer": {
                "$ref": "urn:schema:ss_birth_day_integer"
            },
            "ss_other_given_names_string": {
                "$ref": "urn:schema:ss_other_given_names_string"
            },
            "ss_birth_year_integer": {
                "$ref": "urn:schema:ss_birth_year_integer"
            },
            "ss_family_name_string": {
                "$ref": "urn:schema:ss_family_name_string"
            },
            "ss_first_given_name_string": {
                "$ref": "urn:schema:ss_first_given_name_string"
            }
        },
        "required": [
            "first_given_name",
            "family_name",
            "birth_year",
            "birth_month",
            "birth_day",
            "vaccination_proof"
        ]
    }
}