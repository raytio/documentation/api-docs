{
    "schema_name": "ss_Onboard_NZ_Company",
    "schema_version": "0.2.5",
    "schema_type": "ss",
    "schema": {
        "properties": {
            "org_name": {
                "examples": [
                    "ABC Ltd"
                ],
                "title": "Company Name",
                "priority": 10,
                "type": "string",
                "tags": [
                    "group:org_details"
                ],
                "description": "The name of the Company"
            },
            "org_email": {
                "priority": 20,
                "title": "Company email",
                "type": "string",
                "description": "The email address of the Company",
                "tags": [
                    "group:org_details"
                ],
                "examples": [
                    "welcome@abc.net"
                ]
            },
            "locality": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_location_locality_string"
                    },
                    {
                        "priority": 70,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "route": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_location_route_string"
                    },
                    {
                        "priority": 50,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "street_number": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_location_street_number_string"
                    },
                    {
                        "priority": 40,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "postal_code": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_location_postal_code_string"
                    },
                    {
                        "priority": 80,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "country": {
                "type": "string",
                "title": "Location country",
                "description": "The country of this location",
                "tags": [
                    "group:address",
                    "display:no_autofill"
                ],
                "lookup": "{API_BASE_URL}/db/v1/fnd_countries?select=key:iso_country_code_2,value:country_name",
                "priority": 90
            },
            "sublocality": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_location_sublocality_string"
                    },
                    {
                        "priority": 60,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "picture": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_File"
                    },
                    {
                        "priority": 30,
                        "tags": [
                            "org_details"
                        ],
                        "title": "Logo",
                        "description": "The Company's logo image",
                        "contentMediaType": "image/*",
                        "contentEncoding": "base64",
                        "override": {
                            "permissions": {
                                "default": "Public"
                            }
                        }
                    }
                ]
            }
        },
        "definitions": {
            "ss_nz_location_sublocality_string": {
                "$ref": "urn:schema:ss_nz_location_sublocality_string"
            },
            "ss_nz_location_locality_string": {
                "$ref": "urn:schema:ss_nz_location_locality_string"
            },
            "ss_nz_location_route_string": {
                "$ref": "urn:schema:ss_nz_location_route_string"
            },
            "ss_nz_location_street_number_string": {
                "$ref": "urn:schema:ss_nz_location_street_number_string"
            },
            "ss_nz_location_postal_code_string": {
                "$ref": "urn:schema:ss_nz_location_postal_code_string"
            },
            "ss_File": {
                "$ref": "urn:schema:ss_File"
            }
        },
        "type": "object",
        "description": "Enter the details of the Company",
        "required": [
            "org_name",
            "org_email",
            "location_type",
            "street_number",
            "route",
            "locality",
            "country"
        ],
        "$schema": "http://json-schema.org/draft-07/schema#",
        "tags": [
            "type:client_only"
        ],
        "title": "NZ Company setup",
        "onboard_properties": {
            "organizations": [
                {
                    "properties": {
                        "address": {
                            "city": "{locality}",
                            "street1": "{street_number} {route}",
                            "region": "{postal_code}",
                            "country": "{country}",
                            "street2": "{sublocality}"
                        },
                        "name": "{org_name}",
                        "email": "{org_email}"
                    },
                    "access_applications": [
                        {
                            "properties": {
                                "name": "{org_name} identity verification",
                                "callback_uri": [
                                    "https://app-dev.rayt.io",
                                    "https://app.rayt.io"
                                ],
                                "picture": "{picture}",
                                "aa_introduction": "Welcome to {org_name}. We use Raytio as our identity verification provider.",
                                "scopes": [
                                    "ss_Global_Identity_Document",
                                    "ss_Person_Contact_Address",
                                    "ss_Global_PEP"
                                ]
                            },
                            "links": [
                                {
                                    "description": "Standard CDD (no biometric)",
                                    "wizardConfig": {
                                        "pages": [
                                            {
                                                "name": "Your identity document",
                                                "schemas": [
                                                    "ss_Global_Identity_Document"
                                                ],
                                                "multiple": false,
                                                "filter": "oneOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both",
                                                "allow_upload": true,
                                                "extract_threshold": 0.7,
                                                "extract_threshold_pass_action": "next_step",
                                                "extract_threshold_fail_action": "review",
                                                "verify": true
                                            },
                                            {
                                                "name": "Your address",
                                                "schemas": [
                                                    "ss_Person_Contact_Address"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            },
                                            {
                                                "name": "Your birth information",
                                                "schemas": [
                                                    "ss_Global_PEP"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            }
                                        ],
                                        "review_text": "Please review the data you are about to share and confirm all of the details are correct",
                                        "expiry_date": 365,
                                        "update_reference": false,
                                        "emails": [
                                            "{org_email}"
                                        ],
                                        "quick_onboard": false
                                    }
                                },
                                {
                                    "description": "Standard CDD (biometric)",
                                    "wizardConfig": {
                                        "pages": [
                                            {
                                                "name": "Your identity document",
                                                "schemas": [
                                                    "ss_Global_Identity_Document"
                                                ],
                                                "multiple": false,
                                                "filter": "oneOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both",
                                                "allow_upload": true,
                                                "extract_threshold": 0.7,
                                                "extract_threshold_pass_action": "next_step",
                                                "extract_threshold_fail_action": "review",
                                                "verify": true
                                            },
                                            {
                                                "name": "Your address",
                                                "schemas": [
                                                    "ss_Person_Contact_Address"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            },
                                            {
                                                "name": "Your birth information",
                                                "schemas": [
                                                    "ss_Global_PEP"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            },
                                            {
                                                "name": "Live person check",
                                                "schemas": [
                                                    "ss_Live_Person"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            },
                                            {
                                                "name": "Match person to identity document",
                                                "schemas": [
                                                    "ss_Identity_Binding"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            }
                                        ],
                                        "review_text": "Please review the data you are about to share and confirm all of the details are correct",
                                        "expiry_date": 365,
                                        "update_reference": false,
                                        "emails": [
                                            "{org_email}"
                                        ],
                                        "quick_onboard": false
                                    }
                                }
                            ]
                        }
                    ]
                }
            ],
            "profile_objects": [
                {
                    "schema_name": "ss_NZ_Company",
                    "properties": {
                        "nz_legal_entity_name": "{org_name}",
                        "entity_type_description": "LTD"
                    }
                },
                {
                    "schema_name": "ss_Contact_Address",
                    "properties": {
                        "locality": "{locality}",
                        "street_number": "{street_number}",
                        "route": "{route}",
                        "postal_code": "{postal_code}",
                        "country": "{country}",
                        "sublocality": "{sublocality}"
                    }
                }
            ],
            "relationships": [
                {
                    "from": "{n_id:profile_objects[0]}",
                    "to": "{n_id:profile_objects[2]}",
                    "type": "HAS_ADDRESS",
                    "properties": {
                        "name": "related_address"
                    }
                }
            ],
            "return_to": "/profile/ss_NZ_Company/{n_id:profile_objects[0]}"
        }
    }
}