{
    "schema_name": "ss_Onboard_NZ_Accountant",
    "schema_version": "0.2.2",
    "schema_type": "ss",
    "schema": {
        "$id": "{API_BASE_URL}/graph/v1/schema/ss/ss_Onboard_NZ_Accountant",
        "properties": {
            "org_name": {
                "examples": [
                    "ABC Accounting"
                ],
                "title": "Organisation Name",
                "priority": 10,
                "type": "string",
                "tags": [
                    "group:org_details"
                ],
                "description": "The name of the organisation"
            },
            "org_email": {
                "priority": 20,
                "title": "Organisation email",
                "type": "string",
                "description": "The email address of the organisation",
                "tags": [
                    "group:org_details"
                ],
                "examples": [
                    "welcome@abcaccounting.nz"
                ]
            },
            "locality": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_location_locality_string"
                    },
                    {
                        "priority": 70,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "route": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_location_route_string"
                    },
                    {
                        "priority": 50,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "street_number": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_location_street_number_string"
                    },
                    {
                        "priority": 40,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "postal_code": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_location_postal_code_string"
                    },
                    {
                        "priority": 80,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "country": {
                "type": "string",
                "title": "Location country",
                "description": "The country of this location",
                "tags": [
                    "group:address",
                    "display:no_autofill"
                ],
                "lookup": "{API_DOCS_URL}/lookups/countries.json",
                "priority": 90
            },
            "sublocality": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_location_sublocality_string"
                    },
                    {
                        "priority": 60,
                        "tags": [
                            "group:address",
                            "display:no_autofill"
                        ]
                    }
                ]
            },
            "picture": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_File"
                    },
                    {
                        "priority": 30,
                        "tags": [
                            "org_details"
                        ],
                        "title": "Logo",
                        "description": "The organisation's logo image",
                        "contentMediaType": "image/*",
                        "contentEncoding": "base64",
                        "override": {
                            "permissions": {
                                "default": "Public"
                            }
                        }
                    }
                ]
            }
        },
        "definitions": {
            "ss_nz_location_sublocality_string": {
                "$ref": "urn:schema:ss_nz_location_sublocality_string"
            },
            "ss_nz_location_locality_string": {
                "$ref": "urn:schema:ss_nz_location_locality_string"
            },
            "ss_nz_location_route_string": {
                "$ref": "urn:schema:ss_nz_location_route_string"
            },
            "ss_nz_location_street_number_string": {
                "$ref": "urn:schema:ss_nz_location_street_number_string"
            },
            "ss_nz_location_postal_code_string": {
                "$ref": "urn:schema:ss_nz_location_postal_code_string"
            },
            "ss_File": {
                "$ref": "urn:schema:ss_File"
            }
        },
        "type": "object",
        "description": "Enter the details of the accountancy practice",
        "required": [
            "org_name",
            "org_email",
            "location_type",
            "street_number",
            "route",
            "locality",
            "country"
        ],
        "$schema": "http://json-schema.org/draft-07/schema#",
        "tags": [
            "type:client_only"
        ],
        "title": "NZ Accountant setup",
        "onboard_properties": {
            "organizations": [
                {
                    "properties": {
                        "address": {
                            "city": "{locality}",
                            "street1": "{street_number} {route}",
                            "region": "{postal_code}",
                            "country": "{country}",
                            "street2": "{sublocality}"
                        },
                        "name": "{org_name}",
                        "email": "{org_email}"
                    },
                    "access_applications": [
                        {
                            "properties": {
                                "name": "{org_name} identity verification",
                                "callback_uri": [
                                    "https://app-dev.rayt.io",
                                    "https://app.rayt.io"
                                ],
                                "picture": "{picture}",
                                "service_provider_n_id": "{n_id:profile_objects[0]}",
                                "aa_introduction": "Welcome to {org_name}. We use Raytio as our identity verification provider.",
                                "scopes": [
                                    "ss_Global_Identity_Document",
                                    "ss_Person_Contact_Address",
                                    "ss_Global_PEP",
                                    "ss_Live_Person",
                                    "ss_Identity_Binding"
                                ]
                            },
                            "links": [
                                {
                                    "description": "Standard CDD (no biometric)",
                                    "wizardConfig": {
                                        "pages": [
                                            {
                                                "name": "Your identity document",
                                                "schemas": [
                                                    "ss_Global_Identity_Document"
                                                ],
                                                "multiple": false,
                                                "filter": "oneOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both",
                                                "allow_upload": true,
                                                "extract_threshold": 0.7,
                                                "extract_threshold_pass_action": "next_step",
                                                "extract_threshold_fail_action": "review",
                                                "verify": true
                                            },
                                            {
                                                "name": "Your address",
                                                "schemas": [
                                                    "ss_Person_Contact_Address"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            },
                                            {
                                                "name": "Your birth information",
                                                "schemas": [
                                                    "ss_Global_PEP"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            }
                                        ],
                                        "review_text": "Please review the data you are about to share and confirm all of the details are correct",
                                        "expiry_date": 365,
                                        "update_reference": false,
                                        "emails": [
                                            "{org_email}"
                                        ],
                                        "quick_onboard": false
                                    }
                                },
                                {
                                    "description": "Standard CDD (no biometric) - in person",
                                    "wizardConfig": {
                                        "pages": [
                                            {
                                                "name": "Your identity document",
                                                "schemas": [
                                                    "ss_Global_Identity_Document"
                                                ],
                                                "multiple": false,
                                                "filter": "oneOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both",
                                                "allow_upload": true,
                                                "extract_threshold": 0.7,
                                                "extract_threshold_pass_action": "next_step",
                                                "extract_threshold_fail_action": "review",
                                                "verify": true
                                            },
                                            {
                                                "name": "Your address",
                                                "schemas": [
                                                    "ss_Person_Contact_Address"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            },
                                            {
                                                "name": "Your birth information",
                                                "schemas": [
                                                    "ss_Global_PEP"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            }
                                        ],
                                        "review_text": "Please review the data you are about to share and confirm all of the details are correct",
                                        "expiry_date": 365,
                                        "update_reference": false,
                                        "emails": [
                                            "{org_email}"
                                        ],
                                        "quick_onboard": true
                                    }
                                },
                                {
                                    "description": "Standard CDD (biometric)",
                                    "wizardConfig": {
                                        "pages": [
                                            {
                                                "name": "Your identity document",
                                                "schemas": [
                                                    "ss_Global_Identity_Document"
                                                ],
                                                "multiple": false,
                                                "filter": "oneOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both",
                                                "allow_upload": true,
                                                "extract_threshold": 0.7,
                                                "extract_threshold_pass_action": "next_step",
                                                "extract_threshold_fail_action": "review",
                                                "verify": true
                                            },
                                            {
                                                "name": "Your address",
                                                "schemas": [
                                                    "ss_Person_Contact_Address"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            },
                                            {
                                                "name": "Your birth information",
                                                "schemas": [
                                                    "ss_Global_PEP"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            },
                                            {
                                                "name": "Live person check",
                                                "schemas": [
                                                    "ss_Live_Person"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            },
                                            {
                                                "name": "Match person to identity document",
                                                "schemas": [
                                                    "ss_Identity_Binding"
                                                ],
                                                "multiple": false,
                                                "filter": "anyOf",
                                                "optional": false,
                                                "profile": true,
                                                "service_provider_link": false,
                                                "display_schema_title": false,
                                                "display_schema_description": false,
                                                "display_field_title": true,
                                                "display_field_description": true,
                                                "share": "both"
                                            }
                                        ],
                                        "review_text": "Please review the data you are about to share and confirm all of the details are correct",
                                        "expiry_date": 365,
                                        "update_reference": false,
                                        "emails": [
                                            "{org_email}"
                                        ],
                                        "quick_onboard": false
                                    }
                                }
                            ]
                        }
                    ]
                }
            ],
            "profile_objects": [
                {
                    "schema_name": "ss_Provider_Profile",
                    "properties": {
                        "provider_name": "{org_name}",
                        "picture": "{picture}",
                        "permissions": "Public"
                    }
                }
            ]
        }
    }
}