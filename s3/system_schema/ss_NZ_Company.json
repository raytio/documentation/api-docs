{
    "schema_name": "ss_NZ_Company",
    "schema_version": "0.1.12",
    "schema_type": "ss",
    "schema": {
        "relationships": [
            {
                "direction": "from",
                "type": "HAS_RELATED_DATA",
                "oneOf": [
                    "instance"
                ],
                "multiple": true
            },
            {
                "direction": "from",
                "relationship_name": "director",
                "type": "HAS_RELATED_PARTY",
                "oneOf": [
                    "ss_Person_Name"
                ],
                "multiple": true,
                "required": [
                    "role_type",
                    "start_date"
                ],
                "properties": {
                    "role_type": {
                        "type": "string",
                        "enum": [
                            "Director"
                        ]
                    },
                    "role_status": {
                        "type": "string",
                        "enum": [
                            "ACTIVE"
                        ]
                    },
                    "start_date": {
                        "type": "string",
                        "format": "date",
                        "default": 0
                    },
                    "end_date": {
                        "type": "string",
                        "format": "date",
                        "default": 3650
                    },
                    "independent": {
                        "type": "boolean",
                        "default": true
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "advisor",
                "type": "HAS_RELATED_PARTY",
                "anyOf": [
                    "ss_Person_Name",
                    "ss_NZ_Company"
                ],
                "multiple": true,
                "required": [
                    "appointment_date"
                ],
                "properties": {
                    "appointment_date": {
                        "type": "string",
                        "format": "date",
                        "default": 0
                    },
                    "independent": {
                        "type": "boolean",
                        "default": true
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "administrator",
                "type": "HAS_RELATED_PARTY",
                "oneOf": [
                    "ss_Person_Name",
                    "ss_NZ_Company"
                ],
                "multiple": false,
                "required": [
                    "appointment_date"
                ],
                "properties": {
                    "appointment_date": {
                        "type": "string",
                        "format": "date",
                        "default": 0
                    },
                    "independent": {
                        "type": "boolean",
                        "default": true
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "shareholder",
                "type": "HAS_RELATED_PARTY",
                "oneOf": [
                    "ss_Person_Name",
                    "ss_NZ_Company"
                ],
                "multiple": true
            },
            {
                "direction": "from",
                "relationship_name": "share_allocation",
                "type": "ALLOCATES_SHARES",
                "oneOf": [
                    "ss_NZ_Share_Allocation"
                ],
                "multiple": true
            },
            {
                "direction": "from",
                "relationship_name": "contact",
                "type": "HAS_CONTACT",
                "oneOf": [
                    "ss_Email_Address",
                    "ss_Phone_Number"
                ],
                "multiple": true,
                "properties": {
                    "contact_type": {
                        "type": "string",
                        "enum": [
                            "email",
                            "phone"
                        ]
                    },
                    "start_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0
                    },
                    "end_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "related_address",
                "type": "HAS_ADDRESS",
                "oneOf": [
                    "ss_Contact_Address"
                ],
                "multiple": true,
                "required": [
                    "address_type",
                    "start_date"
                ],
                "properties": {
                    "address_type": {
                        "type": "string",
                        "lookup": "{API_DOCS_URL}/lookups/nz_entity_address_types.json"
                    },
                    "start_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0
                    },
                    "end_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "linked_accounts",
                "type": "HAS_ACCOUNTS",
                "oneOf": [
                    "ss_Connected_Account_Xero"
                ],
                "multiple": false
            },
            {
                "direction": "from",
                "relationship_name": "tax_details",
                "type": "HAS_TAX_DETAILS",
                "anyOf": [
                    "ss_Tax_Registration_Details"
                ],
                "multiple": true
            }
        ],
        "type": "object",
        "title": "NZ company",
        "tags": [
            "action:verify"
        ],
        "i18n": {
            "en-nz": {
                "administrator": {
                    "title_plural": "Administrators",
                    "description": "Company administrator",
                    "title": "Administrator"
                },
                "director": {
                    "description": "Company director",
                    "title_plural": "Directors",
                    "title": "Director"
                },
                "role": {
                    "description": "The role",
                    "title": "Role",
                    "title_plural": "Roles"
                },
                "role_type": {
                    "description": "The type of role",
                    "title": "Role Type",
                    "title_plural": "Role Type"
                },
                "independent": {
                    "title": "Independent",
                    "content": "Is independent?",
                    "description": "Is the person or organisation independent?"
                },
                "shareholder": {
                    "title_plural": "Shareholders",
                    "description": "Company shareholder",
                    "title": "Shareholder"
                },
                "$schema": {
                    "title": "Company",
                    "description": "NZ company details"
                },
                "nz_legal_entity_name": {
                    "title": "Entity Name",
                    "title_plural": "Entity Names",
                    "description": "The name of this legal entity"
                },
                "nzbn": {
                    "description": "New Zealand Business Number",
                    "title": "NZBN",
                    "title_plural": "NZBNs"
                },
                "contact": {
                    "description": "Company contact",
                    "title_plural": "Contacts",
                    "title": "Contact"
                },
                "related_address": {
                    "description": "Related address",
                    "title_plural": "Related addresses",
                    "title": "Related address"
                },
                "registered_address": {
                    "description": "Registered address",
                    "title_plural": "Registered addresses",
                    "title": "Registered address"
                },
                "service_address": {
                    "description": "Service address",
                    "title_plural": "Service addresses",
                    "title": "Address for service"
                },
                "communications_address": {
                    "description": "Communications address",
                    "title_plural": "Communications addresses",
                    "title": "Address for communications "
                },
                "records_address": {
                    "description": "Records address",
                    "title_plural": "Records addresses",
                    "title": "Address for records"
                },
                "register_address": {
                    "description": "Address for service",
                    "title_plural": "Service addresses",
                    "title": "Address for service"
                },
                "advisor": {
                    "description": "Company advisor",
                    "title": "Advisor",
                    "title_plural": "Advisors"
                },
                "share_allocation": {
                    "description": "Share allocation",
                    "title": "Share allocation",
                    "title_plural": "Share allocations"
                },
                "address_type": {
                    "description": "The purpose for which this address is used",
                    "title": "Address type",
                    "title_plural": "Address types"
                },
                "start_date": {
                    "description": "The start date",
                    "title": "Start date",
                    "title_plural": "Start dates"
                },
                "end_date": {
                    "description": "The end date",
                    "title": "End date",
                    "title_plural": "End dates"
                },
                "group:nz_company": {
                    "title": "Company"
                },
                "tax_details": {
                    "title_plural": "Tax details",
                    "description": "Local and foreign tax registration details",
                    "title": "Tax details"
                },
                "linked_accounts": {
                    "title_plural": "Accounts",
                    "description": "Financial accounting records",
                    "title": "Accounts"
                }
            }
        },
        "definitions": {
            "ss_nz_legal_entity_name_string": {
                "$ref": "urn:schema:ss_nz_legal_entity_name_string"
            },
            "ss_nz_nzbn_string": {
                "$ref": "urn:schema:ss_nz_nzbn_string"
            }
        },
        "properties": {
            "source_register_unique_identifier": {
                "title": "Source register ID",
                "tags": [
                    "group:nz_company"
                ],
                "type": "string",
                "description": "The unique identifier for this entity taken from the source register for this entity type",
                "examples": [
                    "1447962"
                ],
                "priority": 600
            },
            "source_register": {
                "title": "Source register",
                "tags": [
                    "group:nz_company"
                ],
                "type": "string",
                "description": "The name of the source register",
                "examples": [
                    "COMPANY"
                ],
                "priority": 700
            },
            "nz_legal_entity_name": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_legal_entity_name_string"
                    },
                    {
                        "tags": [
                            "group:nz_company"
                        ],
                        "priority": 200
                    }
                ]
            },
            "entity_type_description": {
                "examples": [
                    "NZ Limited Company"
                ],
                "title": "Entity type",
                "description": "The type of legal entity",
                "lookup": "{API_DOCS_URL}/lookups/nz_entity_types.json",
                "type": "string",
                "tags": [
                    "group:nz_company"
                ],
                "priority": 300
            },
            "nzbn": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_nz_nzbn_string"
                    },
                    {
                        "priority": 100,
                        "tags": [
                            "group:nz_company"
                        ]
                    }
                ]
            },
            "entity_status_description": {
                "title": "Entity status",
                "tags": [
                    "group:nz_company"
                ],
                "examples": [
                    "Removed"
                ],
                "type": "string",
                "priority": 400,
                "lookup": "{API_DOCS_URL}/lookups/nz_entity_status.json",
                "description": "The status of the legal entity"
            },
            "registration_date": {
                "title": "Registration Date",
                "tags": [
                    "group:nz_company"
                ],
                "examples": [
                    "Removed"
                ],
                "type": "string",
                "format": "date-time",
                "priority": 500,
                "description": "The date on which the legal entity was first registered"
            },
            "total_shares": {
                "title": "Total shares",
                "tags": [
                    "group:nz_company"
                ],
                "type": "integer",
                "priority": 800,
                "description": "The total number of shares in this company"
            },
            "annual_return_filing_month": {
                "title": "Filing month",
                "tags": [
                    "group:nz_company"
                ],
                "type": "integer",
                "priority": 900,
                "description": "The month in which the annual return must be filed"
            },
            "financial_report_filing_month": {
                "title": "Filing month (financial)",
                "tags": [
                    "group:nz_company"
                ],
                "type": "integer",
                "priority": 1000,
                "description": "The month in which the financial reports must be filed"
            }
        },
        "description": "NZ company details",
        "required": [
            "nz_legal_entity_name"
        ],
        "display": {
            "head_sub": {
                "fields": [
                    "entity_type_description"
                ]
            },
            "head_main": {
                "fields": [
                    "nz_legal_entity_name"
                ]
            },
            "expand": [
                {
                    "fields": [
                        "source_register_unique_identifier",
                        "source_register"
                    ],
                    "label": "Registry details for {nzbn}"
                },
                {
                    "fields": [
                        "registration_date",
                        "annual_return_filing_month",
                        "financial_report_filing_month"
                    ],
                    "label": "Relevant dates"
                }
            ]
        },
        "verified_fields": [
            "nzbn",
            "nz_legal_entity_name",
            "entity_type_description",
            "entity_status_description"
        ],
        "$schema": "http://json-schema.org/draft-07/schema#"
    }
}