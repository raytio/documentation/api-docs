{
    "schema_name": "ss_AA_Webhook",
    "schema_type": "ss",
    "schema_version": "0.0.16",
    "schema": {
        "tags": [
            "type:client_only"
        ],
        "description": "The inbound webhook details that will trigger an action on this Access Application",
        "database": {
            "table": "dsm_access_application_inbound_webhooks",
            "primary_key": "id"
        },
        "properties": {
            "provider_webhook_id": {
                "title": "Webhook name",
                "type": "string",
                "priority": 100,
                "lookup": "{API_BASE_URL}/db/v1/dsm_nodes?labels=cs.%7Bss_Provider_Webhook_Notifications%7D&select=key:id,value:properties->provider_webhook_name",
                "description": "The webhook to use"
            },
            "provider_subscription_credentials": {
                "title": "Webhook credentials",
                "type": "object",
                "properties": {},
                "priority": 200,
                "description": "The webhook subscription credentials"
            },
            "provider_signature_check_enabled": {
                "title": "Enable signature check",
                "type": "boolean",
                "priority": 300,
                "description": "Enabling signature checking will ensure that the webhook is coming from a legitimate source",
                "content": "I want to check the inbound webhook signature",
                "default": false
            },
            "provider_signature_credentials": {
                "title": "Signature credentials",
                "type": "object",
                "properties": {},
                "priority": 400,
                "description": "The webhook signing credentials (if required)"
            },
            "webhook_processing_rules": {
                "description": "The rules to follow when a webhook notification is received",
                "title": "Webhook rules",
                "priority": 500,
                "type": "array",
                "add_row_btn_label": "Add rules",
                "table_empty_message": "There are no rules defined",
                "items": {
                    "type": "object",
                    "properties": {
                        "webhook_filter_source": {
                            "type": "string",
                            "enum": [
                                "body",
                                "header",
                                "request_parameter"
                            ],
                            "priority": 300,
                            "description": "The source of the webhook filter (if required)",
                            "title": "Webhook filter source"
                        },
                        "webhook_filter_ruleset": {
                            "description": "A ruleset defining how the webhook filter will be applied",
                            "type": "object",
                            "properties": {},
                            "priority": 400,
                            "title": "Webhook filter ruleset"
                        },
                        "webhook_field_mapping_ruleset": {
                            "description": "A ruleset defining how the received fields are mapped to the action variables",
                            "type": "object",
                            "properties": {},
                            "priority": 500,
                            "title": "Webhook field mapping ruleset"
                        },
                        "webhook_actions": {
                            "description": "The actions to take when the webhook notification is received",
                            "title": "Webhook actions",
                            "priority": 600,
                            "type": "array",
                            "add_row_btn_label": "Add action",
                            "table_empty_message": "There are no actions defined",
                            "items": {
                                "type": "object",
                                "properties": {
                                    "webhook_action_type": {
                                        "description": "The action to take when the webhook notification is received",
                                        "enum": [
                                            "http_post",
                                            "http_put",
                                            "http_get"
                                        ],
                                        "title": "Webhook action type",
                                        "priority": 100,
                                        "type": "string"
                                    },
                                    "webhook_action_uri": {
                                        "description": "The URL to call as part of this action",
                                        "title": "Webhook action URL",
                                        "priority": 200,
                                        "type": "string",
                                        "format": "uri"
                                    },
                                    "webhook_action_authentication_method": {
                                        "description": "The method used to authenticate the action request",
                                        "enum": [
                                            "none",
                                            "aws_cognito",
                                            "api_key"
                                        ],
                                        "title": "Webhook authentication type",
                                        "priority": 300,
                                        "type": "string"
                                    },
                                    "webhook_action_authentication_parameters": {
                                        "description": "The parameter key/values to pass to the authenticator",
                                        "title": "Webhook authentication type",
                                        "priority": 400,
                                        "type": "string"
                                    },
                                    "webhook_action_payload_template": {
                                        "description": "The template defining how the contents of the payload passed to the action URL",
                                        "title": "Webhook action payload",
                                        "type": "string",
                                        "priority": 500
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "status": {
                "title": "Webhook status",
                "type": "string",
                "priority": 1000,
                "description": "The status of the webhook subscription",
                "readOnly": true
            },
            "date_created": {
                "title": "Webhook creation date",
                "type": "string",
                "format": "date-time",
                "priority": 1100,
                "description": "The date on which the webhook subscription was created",
                "readOnly": true
            },
            "date_updated": {
                "title": "Webhook update date",
                "type": "string",
                "format": "date-time",
                "priority": 1200,
                "description": "The date on which the webhook subscription was updated",
                "readOnly": true
            },
            "wi_id": {
                "title": "Webhook subscription ID",
                "type": "string",
                "priority": 1300,
                "description": "The unique ID for this webhook subscription",
                "readOnly": true
            }
        },
        "type": "object",
        "required": [
            "provider_webhook_id",
            "webhook_field_mapping_ruleset"
        ],
        "title": "Webhook details",
        "title_plural": "Webhooks",
        "$schema": "http://json-schema.org/draft-07/schema#"
    }
}