{
    "schema_name": "ss_File",
    "schema_type": "ss",
    "schema_version": "0.1.9",
    "schema": {
        "tags": [
            "type:client_only"
        ],
        "required": [
            "title"
        ],
        "display": {
            "head_main": {
                "fields": [
                    "title"
                ]
            }
        },
        "properties": {
            "metadata": {
                "properties": {},
                "type": "object",
                "priority": 60,
                "title": "Metadata",
                "description": "Arbitrary metadata about the file"
            },
            "content_encoding": {
                "priority": 150,
                "type": "string",
                "readOnly": true,
                "title": "Content encoding",
                "hidden": true,
                "description": "The content encoding of this file",
                "examples": [
                    "base64"
                ]
            },
            "directory": {
                "title": "Directory",
                "examples": [
                    "company/minutes/2019/"
                ],
                "type": "string",
                "priority": 110,
                "description": "The location of the file in the directory structure"
            },
            "content_language": {
                "examples": [
                    "en/NZ"
                ],
                "title": "Content Language",
                "description": "The language of this content, where applicable",
                "priority": 50,
                "hidden": true,
                "type": "string"
            },
            "tags": {
                "examples": [
                    "Minutes"
                ],
                "priority": 40,
                "type": "array",
                "items": {
                    "type": "string"
                },
                "format": "uri",
                "title": "Tags",
                "description": "An array of user-defined tags to allow grouping of files"
            },
            "description": {
                "examples": [
                    "Company board meeting minutes. Present T Drump, KJ Un, John Lenin"
                ],
                "description": "The description of the file",
                "type": "string",
                "priority": 20,
                "title": "Description",
                "maxLength": 500
            },
            "content_type": {
                "priority": 160,
                "description": "The content type of this file",
                "hidden": true,
                "type": "string",
                "title": "Content type",
                "examples": [
                    "application/pdf"
                ],
                "readOnly": true
            },
            "source_filename": {
                "title": "Source filename",
                "type": "string",
                "description": "The original filename",
                "readOnly": true,
                "examples": [
                    "minutes_2019.pdf"
                ],
                "priority": 120
            },
            "title": {
                "title": "Title",
                "priority": 10,
                "examples": [
                    "Meeting minutes 20191104"
                ],
                "type": "string",
                "description": "The title of the file"
            },
            "encrypt": {
                "priority": 250,
                "description": "Select to encrypt the content of this file",
                "default": true,
                "examples": [
                    true
                ],
                "type": "boolean",
                "content": "Encrypted",
                "title": "Encrypt?"
            },
            "permissions": {
                "type": "string",
                "title": "Listing type",
                "description": "Whether the listing is private or public",
                "examples": [
                    "Public"
                ],
                "enum": [
                    "Public",
                    "Private",
                    "Authenticated Only"
                ],
                "priority": 260
            },
            "content": {
                "contentEncoding": "base64",
                "priority": 30,
                "type": "string",
                "examples": [
                    "/aghgy0236halfksfjujtptgsg"
                ],
                "description": "The base64 encoded content of this file",
                "tags": [
                    "action:client_upload"
                ],
                "title": "Content",
                "lookup": "{API_DOCS_URL}/lookups/mime_type.json"
            },
            "content_length": {
                "readOnly": true,
                "type": "integer",
                "hidden": true,
                "priority": 130,
                "examples": [
                    123456
                ],
                "title": "Content length",
                "description": "The size in bytes of the file"
            },
            "content_hash": {
                "priority": 170,
                "type": "string",
                "description": "The hash of the base64 encoded content of this file",
                "title": "Content hash",
                "readOnly": true
            },
            "last_modified": {
                "title": "Last modified date",
                "priority": 140,
                "format": "date-time",
                "readOnly": true,
                "description": "The date when the file was last modified",
                "examples": [
                    "2019-11-13T20:20:39+00:00"
                ],
                "type": "string"
            }
        },
        "description": "An electronic file",
        "type": "object",
        "$schema": "http://json-schema.org/draft-07/schema#",
        "title": "File"
    }
}