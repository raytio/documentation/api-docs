{
    "schema_name": "ss_AU_WA_DriverLicence",
    "schema_type": "ss",
    "schema_version": "0.2.12",
    "end_date": "2021-10-31",
    "schema": {
        "description": "WA Driver Licence",
        "$schema": "http://json-schema.org/draft-07/schema#",
        "required": [
            "first_given_name",
            "family_name",
            "identity_document_identifier",
            "birth_year",
            "birth_month",
            "birth_day",
            "identity_document_picture_extracted_photo_page",
            "consent"
        ],
        "tags": [
            "action:verify",
            "action:map",
            "link_to:ss_Person_Name:id_document",
            "time:extract:25",
            "time:verify:15",
            "time:persist:30",
            "time:persist_verify:20"
        ],
        "verified_fields": [
            "first_given_name",
            "family_name",
            "identity_document_identifier",
            "birth_year",
            "birth_month",
            "birth_day"
        ],
        "schema_group": "identity_document",
        "i18n": {
            "en-nz": {
                "driver_licence": {
                    "title_plural": "Driver Licences",
                    "description": "Driver Licence",
                    "title": "Driver Licence"
                }
            },
            "en-au": {
                "driver_licence": {
                    "title_plural": "Driver Licences",
                    "description": "Driver Licence",
                    "title": "Driver Licence"
                }
            },
            "en": {
                "driver_licence": {
                    "title_plural": "Driver Licenses",
                    "description": "Driver License",
                    "title": "Driver License"
                },
                "AU": {
                    "title_plural": "Australia",
                    "description": "Australia",
                    "title": "Australia"
                }
            }
        },
        "display": {
            "head_main": {
                "fields": [
                    "identity_document_identifier"
                ]
            },
            "head_sub": {
                "fields": [
                    "first_given_name"
                ]
            }
        },
        "definitions": {
            "ss_birth_day_integer": {
                "$ref": "urn:schema:ss_birth_day_integer"
            },
            "ss_birth_year_integer": {
                "$ref": "urn:schema:ss_birth_year_integer"
            },
            "ss_first_given_name_string": {
                "$ref": "urn:schema:ss_first_given_name_string"
            },
            "ss_other_given_names_string": {
                "$ref": "urn:schema:ss_other_given_names_string"
            },
            "ss_identity_document_identifier_string": {
                "$ref": "urn:schema:ss_identity_document_identifier_string"
            },
            "ss_family_name_string": {
                "$ref": "urn:schema:ss_family_name_string"
            },
            "ss_File": {
                "$ref": "urn:schema:ss_File"
            },
            "ss_birth_month_integer": {
                "$ref": "urn:schema:ss_birth_month_integer"
            }
        },
        "type": "object",
        "title": "WA Driver Licence",
        "$id": "{API_BASE_URL}/graph/v1/schema/ss/ss_AU_WA_DriverLicence",
        "properties": {
            "family_name": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_family_name_string"
                    },
                    {
                        "priority": 50,
                        "tags": [
                            "group:images"
                        ]
                    }
                ]
            },
            "birth_month": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_birth_month_integer"
                    },
                    {
                        "tags": [
                            "group:date_picker:birth",
                            "date_component:month"
                        ],
                        "priority": 70
                    }
                ]
            },
            "first_given_name": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_first_given_name_string"
                    },
                    {
                        "priority": 30,
                        "tags": [
                            "group:images"
                        ]
                    }
                ]
            },
            "consent": {
                "definitions": {},
                "type": "boolean",
                "examples": [
                    true
                ],
                "title": "Consent",
                "$schema": "http://json-schema.org/draft-07/schema#",
                "content": "I confirm that I am authorised to provide the personal details presented and I consent to my information being checked with the document issuer or official record holder via third party systems for the purpose of confirming my identity.",
                "description": "Consent to share data",
                "priority": 5000
            },
            "other_given_names": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_other_given_names_string"
                    },
                    {
                        "priority": 40,
                        "tags": [
                            "group:images"
                        ]
                    }
                ]
            },
            "birth_day": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_birth_day_integer"
                    },
                    {
                        "tags": [
                            "group:date_picker:birth",
                            "date_component:day"
                        ],
                        "priority": 60
                    }
                ]
            },
            "birth_year": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_birth_year_integer"
                    },
                    {
                        "tags": [
                            "group:date_picker:birth",
                            "date_component:year"
                        ],
                        "priority": 80
                    }
                ]
            },
            "identity_document_picture_photo_page": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_File"
                    },
                    {
                        "tags": [
                            "group:images"
                        ],
                        "priority": 200,
                        "description": "A picture file of the driver licence front",
                        "title": "Driver Licence front image"
                    }
                ]
            },
            "identity_document_picture_extracted_photo_page": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_File"
                    },
                    {
                        "priority": 210,
                        "tags": [
                            "group:images",
                            "type:extract_required"
                        ],
                        "contentMediaType": "image/png",
                        "description": "A picture file of the driver licence front",
                        "title": "Driver Licence extracted front image"
                    }
                ]
            },
            "identity_document_identifier": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_identity_document_identifier_string"
                    },
                    {
                        "priority": 10,
                        "tags": [
                            "group:images"
                        ]
                    }
                ]
            }
        }
    }
}