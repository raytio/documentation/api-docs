{
    "schema_name": "ss_Provider_Profile",
    "schema_type": "ss",
    "schema_version": "0.1.25",
    "schema": {
        "definitions": {
            "ss_File": {
                "$ref": "urn:schema:ss_File"
            }
        },
        "display": {
            "head_main": {
                "fields": [
                    "provider_name"
                ]
            },
            "head_sub": {
                "fields": [
                    "principal_activity"
                ]
            },
            "tabular": {
                "fields": [
                    "provider_name",
                    "provider_description",
                    "principal_activity"
                ]
            }
        },
        "description": "A service provider profile",
        "i18n": {
            "en-nz": {
                "billing": {
                    "description": "Billing configuration",
                    "title_plural": "Billing configuration",
                    "title": "Billing configuration"
                },
                "webhook": {
                    "description": "Webhook configuration",
                    "title_plural": "Webhook configuration",
                    "title": "Webhook configuration"
                },
                "verifier": {
                    "description": "Verification configuration",
                    "title_plural": "Verification configuration",
                    "title": "Verification configuration"
                },
                "oauth": {
                    "description": "OAuth configuration",
                    "title_plural": "OAuth configuration",
                    "title": "OAuth configuration"
                },
                "related_address": {
                    "description": "Related address",
                    "title_plural": "Related addresses",
                    "title": "Related address"
                },
                "contact": {
                    "description": "Service provider contact",
                    "title_plural": "Contacts",
                    "title": "Contact"
                },
                "legal_entity": {
                    "description": "Legal entity",
                    "title_plural": "Legal entities",
                    "title": "Legal entity"
                },
                "person": {
                    "description": "Person",
                    "title_plural": "People",
                    "title": "Person"
                },
                "relationship_type": {
                    "description": "Relationship",
                    "title_plural": "Relationships",
                    "title": "Relationship"
                },
                "start_date": {
                    "description": "Start date",
                    "title_plural": "Start dates",
                    "title": "Start date"
                },
                "end_date": {
                    "description": "End date",
                    "title_plural": "End dates",
                    "title": "End date"
                }
            }
        },
        "relationships": [
            {
                "direction": "from",
                "relationship_name": "billing",
                "type": "BILLED_USING",
                "oneOf": [
                    "ss_Provider_Billing"
                ],
                "multiple": false
            },
            {
                "direction": "from",
                "relationship_name": "verifier",
                "type": "VERIFICATION_PROVIDER",
                "oneOf": [
                    "ss_Provider_Verification_Provider"
                ],
                "multiple": false
            },
            {
                "direction": "from",
                "relationship_name": "webhook",
                "type": "WEBHOOK_NOTIFICATIONS",
                "oneOf": [
                    "ss_Provider_Webhook_Notifications"
                ],
                "multiple": true
            },
            {
                "direction": "from",
                "relationship_name": "oauth",
                "type": "OAUTH_PROVIDER",
                "oneOf": [
                    "ss_Provider_OAuth_Configuration"
                ],
                "multiple": true
            },
            {
                "direction": "from",
                "relationship_name": "contact",
                "type": "HAS_CONTACT",
                "anyOf": [
                    "ss_Email_Address",
                    "ss_Phone_Number"
                ],
                "multiple": true,
                "properties": {
                    "start_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0,
                        "priority": 100
                    },
                    "end_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0,
                        "priority": 200
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "related_address",
                "type": "HAS_ADDRESS",
                "oneOf": [
                    "ss_Contact_Address"
                ],
                "multiple": true,
                "required": [
                    "address_type",
                    "start_date"
                ],
                "properties": {
                    "address_type": {
                        "type": "string",
                        "lookup": "{API_DOCS_URL}/lookups/nz_entity_address_types.json",
                        "priority": 100
                    },
                    "start_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0,
                        "priority": 200
                    },
                    "end_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0,
                        "priority": 200
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "person",
                "type": "HAS_PERSON",
                "anyOf": [
                    "ss_Person_Name"
                ],
                "multiple": true,
                "required": [
                    "relationship_type"
                ],
                "properties": {
                    "relationship_type": {
                        "type": "string",
                        "enum": [
                            "employee",
                            "contractor"
                        ],
                        "priority": 100
                    },
                    "start_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0,
                        "priority": 200
                    },
                    "end_date": {
                        "type": "string",
                        "format": "date-time",
                        "default": 0,
                        "priority": 300
                    }
                }
            },
            {
                "direction": "from",
                "relationship_name": "legal_entity",
                "type": "IS_LEGAL_ENTITY",
                "oneOf": [
                    "ss_NZ_Company",
                    "ss_NZ_Trust"
                ],
                "multiple": false
            }
        ],
        "properties": {
            "provider_name": {
                "type": "string",
                "title": "Service provider name",
                "description": "The name of the service provider",
                "examples": [
                    "Global Exclusive"
                ],
                "priority": 100,
                "tags": [
                    "group:provider_profile"
                ]
            },
            "provider_description": {
                "type": "string",
                "title": "Provider description",
                "description": "Further information about the provider",
                "examples": [
                    "Global Exclusive is a boutique service provider, supplying ethically-sourced services to people with more money than time"
                ],
                "priority": 200,
                "tags": [
                    "group:provider_profile"
                ]
            },
            "picture": {
                "allOf": [
                    {
                        "$ref": "#/definitions/ss_File"
                    },
                    {
                        "priority": 250,
                        "tags": [
                            "group:provider_profile"
                        ],
                        "title": "Provider image",
                        "description": "A picture to show on the service provider profile",
                        "contentMediaType": "image/*",
                        "contentEncoding": "base64",
                        "override": {
                            "permissions": {
                                "default": "Public"
                            }
                        }
                    }
                ]
            },
            "website": {
                "type": "string",
                "title": "Website",
                "description": "The website of this provider",
                "priority": 280,
                "tags": [
                    "group:provider_profile"
                ]
            },
            "principal_activity": {
                "type": "string",
                "title": "Principal activity",
                "description": "The main business activity of this provider",
                "lookup": "{API_DOCS_URL}/lookups/cascader/bikes.json",
                "examples": [
                    "Manufacture of parts and accessories for motor vehicles"
                ],
                "priority": 300,
                "tags": [
                    "group:provider_profile",
                    "display:cascade"
                ]
            },
            "secondary_activities": {
                "type": "array",
                "items": {
                    "type": "string"
                },
                "title": "Secondary activities",
                "description": "The secondary business activities of this provider",
                "examples": [
                    "Manufacturer of bodies (coachwork) for motor vehicles; manufacture of trailers and semi-trailers"
                ],
                "priority": 400,
                "tags": [
                    "group:provider_profile"
                ]
            },
            "permissions": {
                "type": "string",
                "title": "Permissions",
                "description": "Default permissions for this record",
                "examples": [
                    "Public"
                ],
                "enum": [
                    "Public",
                    "Private",
                    "Authenticated Only"
                ],
                "priority": 500,
                "tags": [
                    "group:provider_profile"
                ]
            },
            "quote_product_enabled": {
                "type": "boolean",
                "title": "Quoting enabled",
                "description": "Allow users to request a quote from this service provider",
                "default": false,
                "priority": 550,
                "tags": [
                    "group:provider_profile",
                    "display:quoting"
                ]
            },
            "provider_rating": {
                "type": "array",
                "title": "Provider rating information",
                "description": "Third party rating details for this provider",
                "priority": 600,
                "tags": [
                    "group:provider_profile"
                ],
                "items": {
                    "type": "object",
                    "properties": {
                        "rating_score": {
                            "type": "string",
                            "title": "Rating score",
                            "description": "The details of the rating e.g. the customer satisfaction score out of 10 or the financial stability rating",
                            "examples": [
                                "8.5"
                            ],
                            "priority": 100
                        },
                        "rating_details": {
                            "type": "string",
                            "title": "Rating details",
                            "description": "Information about the rating such as the methodology, what is a good versus a bad rating etc",
                            "examples": [
                                "The Rating Corp NPS is a score from 0-10 based on regular random surveys of the provider's customers"
                            ],
                            "priority": 200
                        },
                        "rating_provider_details": {
                            "type": "string",
                            "title": "Rating provider details",
                            "description": "Information about the rating provider",
                            "examples": [
                                "Rating Corp is the world's largest independant rating provider to the toothpick industry"
                            ],
                            "priority": 300
                        }
                    }
                }
            }
        },
        "required": [
            "provider_name"
        ],
        "tags": [
            "type:marketplace",
            "type:provider_profile",
            "type:service_provider"
        ],
        "title": "Provider profile",
        "title_plural": "Service Providers",
        "$schema": "http://json-schema.org/draft-07/schema#",
        "type": "object"
    }
}