Raytio API documentation in OpenAPI 3.0 format

## API documentation

To ensure all the dependencies are installed, run

```
poetry install
yarn install
```

Then, since swagger2openapi is a command line tool, you need to install it globally so run

```
npm install -g swagger2openapi
```

Then, combine the different API docs using combine_docs.py run (for more info see below)

```
source .venv/bin/activate
python3 ./scripts/combine_docs.py
```

To generate the API docs in static form

```
npx @redocly/cli build-docs merged_openapi.yaml --template ./s3/template_raytio.hbs --output ./s3/index.html --title "Raytio API documentation"
```

## Email Templates

The layout is defined in special `__layout.html` file, which includes `{{main}}`, [`{{header}}`], [`{{footer}}`] and [`{{bodyLogo}}`](if custom header exists).

If `{{header}}`, `{{footer}}` and `{{bodyLogo}}` don't exist they each have a fallback string to display instead.

Every other HTML file in the folder gets converted into a JSON file in the `./s3/templates` directory when you run `yarn build` (or `yarn build:emails`)

Within each email, the `<subject></subject>` and `<pre></pre>` tags are special, they define the subject of the email and the plaintext fallback (e.g. for email notifications).

Prettier is configured to format the HTML files, you can also trigger this by running `yarn format`. Note that the CI pipeline is configured to auto-format the HTML files so it is not normally necessary to run `yarn format`.

## Categories

The source file is located in `categories/input.json`. This configuration file lives in `categories/config.jsonc`.

To build, run `yarn build` (or `yarn build:categories`)

## Deployment

To upload to S3 (dev):

```
aws s3 sync ./s3 s3://raytio-dev-docs
```

To upload to S3 (prod):

```
aws s3 sync ./s3 s3://raytio-docs
```

## Combine docs

Combine docs is a script that combines the manually maintained oas3.yaml (OpenAPI 3.0) file with the generated postgrest.yaml file for the db endpoints (Swagger 2.0) into a single OpenAPI 3.0 file to allow for one unified HTML file to be generated containing all the API documentation.

### To maintain the prefix_tags in the combine_docs.py script:

- Each of them is a key, value pair that maps from each prefix in persist for raytio custom to the title of the category at the top of the SQL file.
