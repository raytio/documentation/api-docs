import os
# import requests
import json

directory_name = (
    input("Enter the directory path (../s3/system_schema): ") or "../s3/system_schema"
)

try:
    for file in os.listdir(directory_name):
        filename = os.fsdecode(file)
        # print(directory_name)
        # print(filename)
        # if filename == "ss_Provider_Webhook_Notification.json":
        if filename.endswith(".json"):
            # print(os.path.join(directory_name, filename))
            with open(os.path.join(directory_name, filename), "r") as f:
                # contents = f.read()
                # contents_object = json.loads(contents)
                contents_object = json.load(f)
                original_version = contents_object.get("schema_version", "0.0.0")
                version_array = original_version.split('.')
                version_array[2] = str(int(version_array[2]) + 1)
                updated_version = '.'.join(version_array)
                # print(f"original_version = {original_version}")
                # print(f"updated_version = {updated_version}")
                contents_object["schema_version"] = updated_version
                print(f"contents_object before pop = {contents_object}")
                contents_object["schema"].pop("$id", None)
                print(f"contents_object after pop = {contents_object}")
            with open(os.path.join(directory_name, filename), "w") as f:
                # updated_contents = json.dumps(contents_object)
                json.dump(contents_object, f, indent=4)
                # try:
                #     r = requests.request(
                #         "POST", api_url, data=contents, headers=headers
                #     )
                #     if r.status_code != 200:
                #         print(f"r.status_code = {r.status_code} for {filename}")
                #     else:
                #         response_json = r.json()
                #         print(f"request response = {response_json}")
                # except Exception as e:
                #     print(f"Error {e}")
        else:
            print(f"skipping file {filename}")
except Exception as e:
    print(f"Error {e}")
