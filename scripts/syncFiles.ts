import { join } from "path";
import { format } from "prettier";
import fetch from "node-fetch";
import { promises as fs } from "fs";

const SYNC_BRANCH = "sync-files";
const MAINLINE_BRANCH = "master";
const PR_LABELS = ["Dependencies", "Ready to review"];

/**
 * A script regullaly runs to see if our copy of these files are out of date.
 * If so, it will open a PR.
 */
const FILES_TO_SYNC = {
  "s3/lookups/green_card_keys.json":
    "https://raw.githubusercontent.com/lovasoa/sanipasse/master/src/assets/Digital_Green_Certificate_Signing_Keys.json",
  "s3/lookups/eudcc_swedish_trust_cert.pub":
    "https://dgcg.covidbevis.se/tp/cert",
  "s3/lookups/eudcc_swedish_trust_list":
    "https://dgcg.covidbevis.se/tp/trust-list",
  "s3/lookups/nzcp.identity.health.nz.json":
    "https://nzcp.identity.health.nz/.well-known/did.json",
  "s3/lookups/nzcp.covid19.health.nz.json":
    "https://nzcp.covid19.health.nz/.well-known/did.json",
};

const { CI_PROJECT_ID, GITLAB_BOT_TOKEN } = process.env;

const DATE = new Date().toISOString().split("T")[0];

type PR = { source_branch: string }; // eslint-disable-line camelcase

async function gitlabApi(endpoint: string, data?: unknown) {
  const resp = await fetch(
    `https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/${endpoint}`,
    {
      ...(data ? { method: "POST", body: JSON.stringify(data) } : {}),

      headers: {
        "PRIVATE-TOKEN": GITLAB_BOT_TOKEN as string,
        "Content-Type": "application/json",
      },
    }
  ).then((r) => r.json());

  // we also check if there is only one key beacuse "message" is also an attribute returned by the commit API...
  if ((resp.message || resp.error) && Object.keys(resp).length === 1) {
    throw new Error(resp.message || resp.error);
  }

  return resp;
}

async function createPR(changedFiles: Record<string, string>) {
  if (!CI_PROJECT_ID) throw new Error("CI_PROJECT_ID not set");
  if (!GITLAB_BOT_TOKEN) throw new Error("GITLAB_BOT_TOKEN not set");

  console.log("Creating commit...");
  await gitlabApi("/repository/commits", {
    branch: SYNC_BRANCH,
    start_branch: MAINLINE_BRANCH,
    commit_message: `Sync files ${DATE}`,
    author_name: "Raytio Bot",
    author_email: "bot@rayt.io",
    force: true,
    actions: Object.entries(changedFiles).map(([path, content]) => ({
      action: "update",
      file_path: path,
      content,
    })),
  });

  console.log("Checking existing PRs...");
  const currentPRs: PR[] = await gitlabApi("/merge_requests?state=opened");
  const PRAlreadyOpen = currentPRs.some((x) => x.source_branch === SYNC_BRANCH);

  if (PRAlreadyOpen) {
    console.log("PR Already open.");
    return;
  }

  console.log("Creating PR...");
  await gitlabApi("/merge_requests", {
    source_branch: SYNC_BRANCH,
    target_branch: MAINLINE_BRANCH,
    title: `Sync files ${DATE}`,
    description: "Some synced files need to be updated!",
    labels: PR_LABELS.join(","),
    remove_source_branch: true,
    squash: true,
  });
}

async function main() {
  console.log("Fetching files...");

  const changedFileContents: Record<string, string> = {};

  for (const file in FILES_TO_SYNC) {
    console.log("\t", file);

    const url: string = FILES_TO_SYNC[file];
    const fullPath = join(__dirname, "..", file);

    const req = await fetch(url);
    const isJson = url.endsWith(".json"); // can't use Content-Type since some CDNs don't specify it properly.

    const currentFile = await fs.readFile(fullPath, "utf8").catch(() => "");

    if (isJson) {
      const stringified = format(JSON.stringify(await req.json()), {
        parser: "json",
      });
      await fs.writeFile(fullPath, stringified);
    } else {
      const buff = Buffer.from(await req.arrayBuffer());
      await fs.writeFile(fullPath, buff);
    }

    const newFile = await fs.readFile(fullPath, "utf8").catch(() => "");

    if (currentFile !== newFile) {
      console.log("\t\tUpdated");
      changedFileContents[file] = newFile;
    }
  }

  if (Object.keys(changedFileContents).length) {
    console.log("Some files have changed");
    await createPR(changedFileContents);
  } else {
    console.log("Nothing to update");
  }
  console.log("Done");
}

main();
