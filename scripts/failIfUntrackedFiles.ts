import { execSync } from "child_process";

// this will fail (exit code 1) if there are any untracked changes in git, e.g.
// if prettier or the email build process modified some files.

const gitStatus = execSync("git status --porcelain").toString();

if (gitStatus.replace("\n", "")) {
  console.error(
    "❌\n\n",
    "Some files are not formatted correctly, or the emails have not been compiled. \n\n",
    "Run `yarn format && yarn build` locally, then commit the changes. \n\n",
    "The invalid files are:\n",
    gitStatus,
    "\n\n❌"
  );
  process.exit(1);
}
