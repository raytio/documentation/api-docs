/**
 * This script generates the .json files in `./s3/templates` from the HTML files in
 * `./s3/emails`
 */

import { promises as fs } from "fs";
import { join } from "path";

const HTML_DIR = join(__dirname, "../emails");
const JSON_DIR = join(__dirname, "../s3/templates");
const LAYOUT_FILE = "__layout.html";

async function processFile(
  fileName: string,
  layoutHtml: string
): Promise<void> {
  const html = await fs.readFile(join(HTML_DIR, fileName), {
    encoding: "utf-8",
  });

  const subject = html.split("<subject>")[1]?.split("</subject")[0];
  if (!subject) {
    throw new Error(`${fileName} does not contain a <subject> tag`);
  }

  const plainText = html.split("<pre>")[1]?.split("</pre")[0];
  if (!plainText) {
    throw new Error(`${fileName} does not contain a <pre> tag`);
  }

  const header = html.split("<header>")[1]?.split("</header")[0];

  // fallback incase no footer is provided
  const footer =
    html.split("<footer>")[1]?.split("</footer")[0] ||
    '<p>© Raytio Ltd | <a href="https://www.rayt.io">Raytio</a></p>';

  // if raytio logo in header then it won't appear in the body
  const bodyLogo = header
    ? html.split("<bodyLogo>")[1]?.split("</bodyLogo")[0]
    : "";

  // stick the html file into the layout, but without the metadata tags
  const finalHtml = layoutHtml
    // stick custom or fallback header into layout
    .replace(
      "{{header}}",
      header ||
        '<img src="https://api-docs.rayt.io/images/raytio_png_logos/raytio-logo-text@128px.png" alt="Raytio logo." width="175px"/>'
    )
    .replace("{{main}}", html) // stick file into layout
    .replace("{{bodyLogo}}", bodyLogo) // if header custom header exists stick raytio logo into layout
    .replace("{{footer}}", footer) // stick custom footer into layout
    .replace(/ +/g, " ") // mutiple spaces have no effect in HTML
    .replace(/(\\n|\n)/g, "") // new lines have no effect in HTML
    .replace(
      /<(subject|pre|footer|header|bodyLogo)>.+<\/(subject|pre|footer|header|bodyLogo)>/g,
      ""
    ) // remove metadata tags
    .replace(/<!--[^>]*-->/g, ""); // remove HTML comments

  const json = {
    __comment: "This file is auto-generated, do not edit it! Edit the HTML",
    html_templated_string: finalHtml,
    text_templated_string: plainText.replace(/^\n/, ""), // remove initial line break
    subject_templated_string: subject,
  };

  await fs.writeFile(
    join(JSON_DIR, fileName.replace(".html", ".json")),
    `${JSON.stringify(json, null, 2)}\n`
  );
}

async function main() {
  await fs.mkdir(JSON_DIR, { recursive: true });

  // load the layout file
  const layoutHtml = await fs.readFile(join(HTML_DIR, LAYOUT_FILE), {
    encoding: "utf-8",
  });

  const htmlFileNames = await fs.readdir(HTML_DIR);

  for (const fileName of htmlFileNames) {
    if (fileName !== LAYOUT_FILE && fileName.endsWith(".html")) {
      await processFile(fileName, layoutHtml);
    }
  }
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
