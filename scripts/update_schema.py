import os
import requests
import json

directory_name = (
    input("Enter the directory path (../s3/system_schema): ") or "../s3/system_schema"
)
api_url = (
    input("Enter the API URL (https://api.rayt.io/db/v1/dsm_schema): ")
    or "https://api.rayt.io/db/v1/dsm_schema"
)
token_file_name = (
    input("Enter the filename containing the API token: (token.txt)") or "token.txt"
)
try:
    with open(token_file_name, "r") as f:
        api_token = f.read().rstrip("\n")
    # print(api_token)
except Exception as e:
    print(f"Error {e}")

headers = {
    "Accept": "application/json",
    "Authorization": "Bearer " + api_token,
}

try:
    for file in os.listdir(directory_name):
        filename = os.fsdecode(file)
        # print(directory_name)
        # print(filename)
        if filename.endswith(".json"):
            print(os.path.join(directory_name, filename))
            with open(os.path.join(directory_name, filename), "r") as f:
                contents = f.read()
                try:
                    r = requests.request(
                        "POST", api_url, data=contents, headers=headers
                    )
                    if r.status_code != 200:
                        print(f"r.status_code = {r.status_code} for {filename}")
                    else:
                        response_json = r.json()
                        print(f"request response = {response_json}")
                except Exception as e:
                    print(f"Error {e}")
        else:
            print(f"skippng file {filename}")
except Exception as e:
    print(f"Error {e}")
