import { promises as fs } from "fs";
import { join } from "path";
import { format } from "prettier";
import { File, Category } from "./types";
import { Replacer } from "./createReplacer";
import { scanner } from "./scanner";

const dbPromise: Promise<Category> = fs
  .readFile(join(__dirname, "../../categories/input.json"), "utf-8")
  .then(JSON.parse);

export const processor = async (
  file: File,
  replacer: Replacer
): Promise<void> => {
  console.log("Processing", file.fileName);
  const rootCategory = await dbPromise;

  const result = scanner(rootCategory, file, replacer)?.children;
  if (!result) throw new Error("Nothing was produced");

  await fs.writeFile(
    join(__dirname, `../../s3/lookups/cascader/${file.fileName}`),
    format(JSON.stringify(result), { parser: "json" })
  );

  if (file.include.length) {
    console.warn(
      "\t(!) Some keys were never scanned:",
      file.include.join(", ")
    );
  }

  console.log("\tdone!");
};
