export type Replacer = (str: string) => string;

export function createReplacer(
  targets: { from: string; to: string }[] | undefined
): Replacer {
  if (!targets) return (x) => x;

  return (str) => targets.reduce((s, { from, to }) => s.replace(from, to), str);
}
