import { File, Category, OutputCategory } from "./types";
import { Replacer } from "./createReplacer";

function convertCat(cat: Category, replacer: Replacer): OutputCategory {
  return {
    value: replacer(cat.Number),
    label: replacer(cat.Name),
  };
}

/**
 * like `array.includes()` but mutates array to remove the item if it exists
 * Because we're processing a huge json file, and this gets called _a lot_ of
 * times, mutation is cheaper (memory-wise) than creating a new array each time.
 */
const includesMutating = <T>(list: T[], item: T): boolean => {
  const index = list.indexOf(item);
  const includes = index !== -1;

  if (includes) list.splice(index, 1);
  return includes;
};

/**
 * recusively scans for properties to keep
 *
 * 🧙‍♂️ Beware! this mutates file.includes
 */
export function scanner(
  category: Category,
  file: File,
  replacer: Replacer,
  shouldKeepAllChildren?: boolean
): OutputCategory | undefined {
  /**
   * whether we want to _keep_ this category/item (either explicitly
   * or because it's the child of one we explictly want to keep)
   */
  const keep =
    shouldKeepAllChildren || includesMutating(file.include, category.Number);

  if (category.Subcategories) {
    const children = category.Subcategories.map((subCategory) =>
      scanner(subCategory, file, replacer, keep)
    ).filter((x): x is OutputCategory => !!x);

    // we don't want anything in this subcategory, so let's not even include it
    if (!children.length) return undefined;

    return {
      ...convertCat(category, replacer),
      children,
    };
  }

  // we have reached the deepest part of the tree
  if (keep) return convertCat(category, replacer);

  // we don't want this category and it's the deepest one
  return undefined;
}
