import { strict as assert } from "assert";
import { Config } from "./types";

export const validateConfig = (c: unknown): Config => {
  /* eslint-disable dot-notation -- waiting until `noPropertyAccessFromIndexSignature` */
  assert(
    c && typeof c === "object" && "outputs" in c && Array.isArray(c["outputs"]),
    "[config] outputs doesn't exist or isn't an array"
  );

  c["outputs"].forEach((o) => {
    assert(
      o &&
        typeof o === "object" &&
        "fileName" in o &&
        typeof o["fileName"] === "string",
      "[config] outputs[] must have { fileName: string }"
    );
    assert(
      "include" in o && Array.isArray(o["include"]),
      "[config] outputs[] must have { include: string[] }"
    );

    o["include"].forEach((i: unknown) =>
      assert(typeof i === "string", "[config] include keys must be a string")
    );

    if (o["exclude"]) {
      assert(
        Array.isArray(o["exclude"]),
        "[config] if exlucde is specified it must be an array"
      );
      o["exclude"].forEach((e) =>
        assert(typeof e === "string", "[config] exclude keys must be a string")
      );
    }
  });

  if (c["substitute"]) {
    assert(
      Array.isArray(c["substitute"]),
      "[config] substitute is not an array"
    );

    c["substitute"].forEach((s) =>
      assert(
        s && typeof s === "object" && "from" in s && "to" in s,
        "[config] substitute[] must have { from, to }"
      )
    );
  }

  return c as Config;
  /* eslint-enable dot-notation */
};
