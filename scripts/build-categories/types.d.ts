/** experimenting with nominal types (microsoft/TypeScript#202) */
type Key = string & { $$typeof$$: "key" };

export interface Category {
  Name: string;
  /** contains hyphens every four characters, with a trailing hyphen */
  Number: Key;
  Path: string;
  Subcategories: Category[];
  AreaOfBusiness: 3;
  IsLeaf: boolean;

  HasClassifieds?: boolean;
}

export interface OutputCategory {
  value: string;
  label: string;
  children?: OutputCategory[];
}

type File = {
  fileName: string;
  include: Key[];
  exclude?: Key[];
};

export type Config = {
  outputs: File[];
  substitute?: { from: string; to: string }[];
};
