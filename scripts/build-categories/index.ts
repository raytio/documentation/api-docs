import { readConfigFile, sys } from "typescript";
import { join } from "path";
import { validateConfig } from "./validateConfig";
import { createReplacer } from "./createReplacer";
import { processor } from "./processor";

// using the TypeScript compiler's API instead of `require()`
// since it can parse jsonc without adding another dependency
const { config: unsafeConfig, error } = readConfigFile(
  join(__dirname, "../../categories/config.jsonc"),
  sys.readFile
);
if (error) throw new Error(JSON.stringify(error.messageText));

const config = validateConfig(unsafeConfig);

const replacer = createReplacer(config.substitute);

async function main() {
  for (const output of config.outputs) {
    await processor(output, replacer);
  }
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
