"""
Script to combine the generated PostgREST and manually maintained OAS3 files into a single OpenAPI file
"""

import requests
import subprocess
import json
import os
import yaml
import datetime

# URL to the PostgREST file
environment = os.getenv('ENVIRONMENT', 'dev')
if environment == 'prod':
    postgrest_url = "https://st.rayt.io"
else:
    postgrest_url = "https://st-dev.rayt.io"


prefix_tags = {
    "prm": "partner relationship model",
    "xrm": "supplier/customer relationship management",
    "qom": "Quote and order management",
    "fnd": "Foundation",
    "far": "Financials - accounts receivable",
    "pcm": "Pricing model",
    "fxm": "Funds transfer management",
    "hrm": "human resources model",
    "gpm": "global product model",
    "pbm": "party billing model",
    "dsm": "data sharing model",
}
        

def retrieve_postgrest_file(url):
    """
    Retrieve the PostgREST file from the given URL
    """
    try:
        response = requests.get(url)
        response.raise_for_status()  # Raise an error for bad responses
        return response.json()  # Assuming the response is in JSON format
    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")
        exit(1)

def convert_swagger_to_openapi(swagger_file_path, output_file_path):
    """
    Convert the Swagger 2.0 file to OpenAPI 3.0 format
    """
    try:
        # Call the swagger2openapi command-line tool
        subprocess.run(['swagger2openapi', swagger_file_path, '-o', output_file_path], check=True)
        print(f"Successfully converted {swagger_file_path} to OpenAPI 3.0 format at {output_file_path}")
    except subprocess.CalledProcessError as e:
        print(f"An error occurred during conversion: {e}")
        exit(1)

def convert_yaml_to_json(yaml_file_path, json_file_path):
    """
    Convert a YAML file to JSON format
    """
    try:
        with open(yaml_file_path, 'r', encoding='utf-8') as yaml_file:
            yaml_content = yaml.safe_load(yaml_file)
        
        with open(json_file_path, 'w', encoding='utf-8') as json_file:
            json.dump(yaml_content, json_file, indent=2, default=DateEncoder)
        
        print(f"Successfully converted {yaml_file_path} to JSON format at {json_file_path}")
    except Exception as e:
        print(f"An error occurred during YAML to JSON conversion: {e}")
        exit(1)
        
def DateEncoder(obj):
    """
    Encode dates to a string format
    """
    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.strftime('%Y-%m-%d')

def add_prefix_tags(json_file_path, output_file_path):
    """
    Add prefix tags to the OpenAPI file
    """
    try:
        with open(json_file_path, 'r', encoding='utf-8') as file:
            data = json.load(file)

        for path, methods in data.get('paths', {}).items():
            for method, details in methods.items():
                if 'tags' in details:
                    updated_tags = []
                    for tag in details['tags']:
                        updated_tags.append(tag)
                        # Check if the tag is in the format xxx_name
                        if '_' in tag:
                            prefix = tag.split('_')[0]
                            if prefix not in updated_tags:
                                updated_tags.append(prefix)
                    details['tags'] = updated_tags

        with open(output_file_path, 'w', encoding='utf-8') as file:
            json.dump(data, file, indent=2)

        print(f"Successfully updated tags in {output_file_path}")

    except Exception as e:
        print(f"An error occurred: {e}")
        exit(1)
def merge_openapi_files(base_json_path, additional_json_path, output_json_path):
    """
    Merge the second OpenAPI file into the first one
    """
    try:
        with open(base_json_path, 'r', encoding='utf-8') as base_file:
            base_data = json.load(base_file)

        with open(additional_json_path, 'r', encoding='utf-8') as additional_file:
            additional_data = json.load(additional_file)

        # Merge paths
        if 'paths' in additional_data:
            base_data['paths'].update(additional_data.get('paths', {}))

        # Merge components
        if 'components' in additional_data:
            for component_type, components in additional_data['components'].items():
                if component_type not in base_data['components']:
                    base_data['components'][component_type] = {}
                base_data['components'][component_type].update(components)

        # Process paths to create x-tagGroups
        base_tag_groups = {group['name']: group for group in base_data.get('x-tagGroups', [])}

        for path, methods in additional_data.get('paths', {}).items():
            for method, details in methods.items():
                if 'tags' in details:
                    for tag in details['tags']:
                        # Determine the prefix for the tag name
                        prefix = tag.split('_')[0]
                        group_title = prefix_tags.get(prefix)

                        if group_title:
                            if group_title in base_tag_groups:
                                # Add tag to existing group if not already present
                                if tag not in base_tag_groups[group_title]['tags']:
                                    base_tag_groups[group_title]['tags'].append(tag)
                            else:
                                # Create a new group with the determined title
                                new_group = {
                                    'name': group_title,
                                    'tags': [tag]
                                }
                                base_data.setdefault('x-tagGroups', []).append(new_group)
                                # Update the base_tag_groups dictionary
                                base_tag_groups[group_title] = new_group

        # Write the merged data to a new JSON file
        with open(output_json_path, 'w', encoding='utf-8') as output_file:
            json.dump(base_data, output_file, indent=2)

        print(f"Successfully merged OpenAPI files into {output_json_path}")

    except Exception as e:
        print(f"An error occurred during merging: {e}")
        exit(1)

def convert_json_to_yaml(json_file_path, yaml_file_path):
    """
    Convert a JSON file to YAML format
    """
    try:
        with open(json_file_path, 'r', encoding='utf-8') as json_file:
            json_content = json.load(json_file)
        
        with open(yaml_file_path, 'w', encoding='utf-8') as yaml_file:
            yaml.dump(json_content, yaml_file, default_flow_style=False)
        
        print(f"Successfully converted {json_file_path} to YAML format at {yaml_file_path}")
    except Exception as e:
        print(f"An error occurred during JSON to YAML conversion: {e}")
        exit(1)

# Retrieve the file
postgrest_data = retrieve_postgrest_file(postgrest_url)

print("Successfully retrieved PostgREST data.")
# Save the retrieved data to a file
swagger_file_path = 'postgrest_swagger.json'
with open(swagger_file_path, 'w', encoding='utf-8') as f:
    json.dump(postgrest_data, f)

# Convert the Swagger 2.0 file to OpenAPI 3.0
openapi_file_path = 'postgrest_openapi.yaml'
convert_swagger_to_openapi(swagger_file_path, openapi_file_path)

# Convert the OpenAPI 3.0 YAML file to JSON
openapi_json_file_path = 'postgrest_openapi.json'
convert_yaml_to_json(openapi_file_path, openapi_json_file_path)

# File paths
input_json_file = 'postgrest_openapi.json'
output_json_file = 'postgrest_openapi_updated.json'

# Update the tags
add_prefix_tags(input_json_file, output_json_file)

# Convert oas3.yaml to JSON
oas3_yaml_path = './s3/oas3.yaml'
oas3_json_path = 'oas3.json'
convert_yaml_to_json(oas3_yaml_path, oas3_json_path)

# File paths
oas3_json_path = 'oas3.json'
postgrest_json_path = 'postgrest_openapi.json' # can also use postgrest_openapi_updated.json
merged_json_path = 'merged_openapi.json'

# Merge the OpenAPI files
merge_openapi_files(oas3_json_path, postgrest_json_path, merged_json_path)

# Convert the merged JSON back to YAML
merged_yaml_path = 'merged_openapi.yaml'
convert_json_to_yaml(merged_json_path, merged_yaml_path)

