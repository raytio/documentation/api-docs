# Email Templates

The layout is defined in special `__layout.html` file, which includes `{{main}}`.

Every other HTML file in the folder gets converted into a JSON file in the `./s3/templates` directory when you run `yarn build`

Within each email, the `<subject></subject>` and `<pre></pre>` tags are special, they define the subject of the email and the plaintext fallback (e.g. for email notifications).

Prettier is configured to format the HTML files, you can also trigger this by running `yarn format`.
